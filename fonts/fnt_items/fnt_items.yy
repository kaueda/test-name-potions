{
    "id": "6935cf72-f5f1-4856-a647-11d07620f73f",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_items",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "87ad84df-8fce-4772-a14c-0f1832e45e9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 120,
                "y": 87
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d5cc6bbd-65c1-49ce-9741-4be9134b91e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 81,
                "y": 104
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "20abf013-cec4-402e-b291-a2b3261f3738",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 108,
                "y": 87
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "055396c4-27ba-432b-8653-62f4c6627397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 82,
                "y": 36
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2eb62141-af3c-490f-9adc-21430a557371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 53
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "08a67c5f-d21b-4280-b5b0-402cc7f2ca4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "07d6df81-5304-4864-9ef7-5779b4a6cb67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 91,
                "y": 19
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "cd5804fd-98c9-4ed5-a556-9063314cc949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 101,
                "y": 104
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "53ffc65d-b0c2-47a3-ae56-a2b3ab92a6fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 44,
                "y": 104
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "26580aac-959d-4545-9fda-073008d09837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 38,
                "y": 104
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ffd4c34e-c43e-4bad-aaf8-26473f584613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 87,
                "y": 87
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f6dd918b-39a0-4e4e-8e6d-0d19c08e9094",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "dc8badac-9091-42db-b401-495fdc601aff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 65,
                "y": 104
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "7418ee51-5c2a-46aa-9e85-c1baba261b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6d704440-823f-4731-b92d-df38f8c7a14c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 73,
                "y": 104
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e7a0f624-d24c-420f-9f12-1bff49c5c7c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 20,
                "y": 104
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "406bb4b6-bd15-4597-b8af-4dc6a443e879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 53
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5af3538b-9941-4df7-b546-3fe587113b9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 8,
                "y": 104
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "6678f4be-b284-4571-b995-ecd1746e686c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 70
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "eb685a2a-65e5-4437-92e5-89802cfd2883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 87
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "428917fd-f8c9-4a52-a0fa-354d8660bf13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 87
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "102775b7-768d-4cbb-96c3-e97d2363dc1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 87
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e9b8fd7b-b116-4b7e-b866-0ab6b73a71c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 83,
                "y": 70
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b6648c5d-c3a6-4fb2-ad96-ad0e3f5a0a79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 70
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "92e729dc-4934-4359-8b35-e11e8db285bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 70
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "25f3037f-800b-4c85-b2a2-b2d9480d0322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 56,
                "y": 70
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e8a87750-e972-425c-936b-85d7b7e20bf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 93,
                "y": 104
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "283d42ba-ebbb-4386-a6dc-780e5fcd6fb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 77,
                "y": 104
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e9136f8b-4f98-41ae-a8dd-fe007cbd0a31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 36
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b2337a05-743f-4c06-be6d-15c24fe6434b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 70
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "39013aff-269e-4492-b5df-ad23df827dcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 70
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "09292059-4558-42a9-8097-25c7e34b6667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 70
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "fd0190fd-b9d0-47d4-9b17-3d7f08261bb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "183fc915-7245-4a23-b609-614b3558750e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6a458c51-933e-46ab-88f4-10eb15895a5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 52,
                "y": 36
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b1b68cd2-d4f6-4545-9f11-de204cf5bb5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 25,
                "y": 19
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "03fd9cfd-d280-4d8e-9975-bf1d3c8e379f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 42,
                "y": 36
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "50b266da-caa9-44c9-b4a8-a2c5824bd063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 110,
                "y": 70
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "7515711b-d541-4d95-a9f0-8cda4bfb035b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 70
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a7c83e1a-d7bd-4224-9869-cebcb9041c14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9e480586-b369-493c-bd95-0963adce0d5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 36
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a4dcae1a-da42-44e3-9580-af593ed3c649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 89,
                "y": 104
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "9f4d0c35-a0bc-40d4-a217-0a3f3e63bc45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 72,
                "y": 87
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "bec96179-87c9-4ff1-b267-5f72cbce183d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 36,
                "y": 19
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "252c5b57-5513-410f-aaee-0f503a7d03e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 87
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "3b6015aa-0773-4862-bc74-ebb34dec9092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ecd2cfd7-a7ad-4fb1-8732-c0b707e6874e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 69,
                "y": 19
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c28fce76-166b-4fe3-a59b-d9f5a6dbcfa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "95b15339-a2b2-4126-9b43-c1785d0c238e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 113,
                "y": 19
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5f0b7f7b-cbfb-44e8-962d-9e3ffbfeb673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b8418ecb-e794-4a9a-9554-3d854f5cf004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 102,
                "y": 19
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "08fe0501-d8d7-43ca-81a3-5ab1289edb82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 36
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "6881ab7d-57e7-4c01-a3ab-e2d9a9f25d27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 36
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9e361620-33d0-4a57-a46e-c94c5ed02627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 36
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "11ce652f-b5ef-433a-a33d-bb34173d5cd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 80,
                "y": 19
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c018e4b9-9bfc-479f-ba4e-1bb12366a325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "bb6e4bc9-9a29-47a0-a815-84d3f2448c78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 14,
                "y": 19
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e3be689c-671d-42b1-aa7b-9c430de16c77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 47,
                "y": 19
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5dec7e95-ca8a-40d2-919c-e60e69e8fc5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "cabf53a5-a8e2-4433-897a-fc790cc13d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 32,
                "y": 104
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "32257c02-26fc-4408-96cc-227619ce4614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 26,
                "y": 104
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6c13b76b-20e6-4213-8167-c4a08016d34b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 60,
                "y": 104
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b263f274-a420-4718-80a9-11037578f8af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 56,
                "y": 87
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ddf7fb22-2e62-42e4-a03a-ebf3a99052db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 58,
                "y": 19
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "350fcdbf-8c3d-42f9-998b-5173f2ec3ea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 55,
                "y": 104
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6ca24d98-2c9d-4033-8fd5-37f0cb2476d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 53
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b88ffc7c-6c9f-4fc5-b14d-4a44da730cca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 53
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f7bad43e-5a6b-4be0-b86c-b711f91251af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 83,
                "y": 53
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b87c6f12-88fc-4fff-bf9d-3e391b7fed1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 92,
                "y": 53
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d01a688a-6552-401b-a2b4-0113e2bee5db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 101,
                "y": 53
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ae2ed785-f8ea-42eb-a6f6-ecb3f6e3ee98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 101,
                "y": 87
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "17896dfc-45b6-4e2b-8338-7805b990e76a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 36
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ff5967eb-7424-468c-8de9-fcdb579ab50d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 87
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ed2b4332-4d34-41ac-b80e-af4a6527262a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 69,
                "y": 104
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "fb4ffbdc-20b6-4ca7-8db3-cc093008f8fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 50,
                "y": 104
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "99323628-5f13-4300-ae62-6275b08326f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 56,
                "y": 53
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d179597b-dc77-47ac-878c-b1f5cb022462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 85,
                "y": 104
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c4f2fbcb-4561-431a-9da5-f75165607229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "20f92032-f9bd-435e-bcc8-d647d89cdc76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 53
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5db7788b-0adf-4137-b09a-7406fa534c16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 53
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ac5e513b-645b-49cc-a24b-d9ecdd96b0da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 53
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f468a67e-3c02-49cc-87a3-7ba8a8d7323c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 53
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f5d22e3a-f875-4860-be64-e7aceacc583f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 80,
                "y": 87
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8830c1ef-6743-4720-a9a8-e0c67d101cad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 64,
                "y": 87
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "96972692-6d70-41e5-9203-4bf847ebce75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 114,
                "y": 87
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6ad9c0a2-65f5-4425-b34d-571593e3a7e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 70
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0c221a5f-9096-41e4-adb3-e46e39de4cc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 101,
                "y": 36
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "2cb23e5e-7549-4cad-913d-b42b67a004f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8ef77d87-dc03-4516-a836-43213c7d0309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9309a79a-2c4b-4ea6-a99e-7a0e22099109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 92,
                "y": 70
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a918199c-e766-4818-a657-6926e530ad55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 87
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "859e77a4-a64b-4c02-9322-155bf2767e30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 94,
                "y": 87
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "24b035d0-cfa7-4625-9f3e-a4f574c391e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 97,
                "y": 104
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0f3a2a21-4a18-4de4-b882-a3d4f037c1fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 14,
                "y": 104
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "193dd872-6b52-47ee-ab2c-7b21964c159a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 72,
                "y": 36
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "ca646993-ad09-4fee-ad66-3b3e0993fca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "609b34c4-755c-4802-bfd4-330394e128c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "0c4174bd-7446-45f0-843e-c9638f516f4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "4c093205-0340-4e7b-bf36-0f135d4792c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "c229f955-7c66-4059-935f-91b5ea7343fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "74bffba7-0548-4c9c-a630-122f719eabbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "5cc61c60-66db-41e2-9ffc-e428483f1c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "f3f95a88-1de3-49c9-b5ea-a5e5d8f657c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "7b9282d4-a7d0-4da9-8d24-0e1d2dc5114d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "c41ca96b-0f17-4f0f-8e41-51977dd10759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "bb3ea75f-065e-4da2-bd13-77b331f0fb73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "ab8fde0a-0ddd-4357-a1b0-31f2883d7e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "b261fc06-c0dd-4395-93d6-9cea6353570d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "29333ee1-7d54-496c-908a-563dba048a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "6078c7e7-a127-48e3-93b6-c77b6a96c4a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "051cdc96-1327-4834-9bb5-2d5bc31ff82d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "e2f4ae49-9db3-4e4d-8942-fc72a3b799af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "b3e117d1-a832-4374-9f61-9e0553f96144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "41494ea4-8969-4397-8b3f-e45075ab7f41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "9b4695eb-6996-4aef-a02a-ee0655346bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "0428f807-f31c-471a-b275-c5b32bc68852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "dd7d55e1-7618-4ff3-ac26-c39a99c30a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "29e59e4e-3864-4cbc-ba6b-da1ce32e377a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "38555983-01e5-49d8-9fe6-812fa0421ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "a681e367-ec50-4647-b043-5733fe95e157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "00942a73-1407-451b-b84f-a28630dcf019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "4fd9324a-72b0-4ad1-a4ae-9d9571b89d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "bf488b50-89b3-4fcb-941a-e3cd41b39466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "42d64821-3321-4b11-8124-d68b724f3290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "16dc193f-5c96-4aa3-b6b7-af2257940408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "9b5ff594-5edf-4ebc-bc79-5f6e877e7744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "23f561ec-79d0-4953-8500-717872b6290d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "c0012bdf-8f6e-4f12-bc5e-06ebd0330cc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "8013be5b-7b78-42b4-9b2a-1bcdf9767e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "a4fc9721-2bad-4c65-b871-0dca95f929e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "c77d2cb2-540e-4e50-9bcb-0f10d8a3249e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "988434ae-b973-4b43-b84d-f242e1a180b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "210fe4ec-3ddf-467d-a5b3-c16c9b555c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "d41d3398-51b1-4bf3-a7ae-bbebfc012a62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "a1365354-fe17-4e99-b613-a6860d5cbc2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "202de505-e473-4502-b219-3d03d3049a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "b77bb8a4-412a-4cbf-b399-9150767bd72b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "f83bb4bd-40d6-4a19-8dcc-954e36f14060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}

{
    "name": "rm_test",
    "id": "694ad6ed-f096-41d4-b63f-ba70a588527a",
    "creationCodeFile": "RoomCreationCode.gml",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "c299eb6d-e276-42e4-a896-b28901767206",
        "f86e9446-f8a1-4915-a8bd-c33034ac3e7d",
        "2fa879ad-ff94-441c-9e64-44c189d50db6",
        "29fa24af-e7a8-48e5-b012-c280bd6847a4",
        "43acdeee-eace-4071-827e-454e5ccc5c26",
        "d0cb6951-9f93-4120-b016-bdcde0e93393"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "a55c4fc6-df30-418b-b5b1-96f6efd06e0e",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_683A8749","id": "2fa879ad-ff94-441c-9e64-44c189d50db6","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_683A8749","objId": "13f642a5-3807-4b09-91a4-a15a954e3cc4","properties": [{"id": "e80f826b-c63b-4833-8f43-83cfbbfb7d0d","modelName": "GMOverriddenProperty","objectId": "13f642a5-3807-4b09-91a4-a15a954e3cc4","propertyId": "4e348254-5b3b-4017-998e-60a128b761a4","mvc": "1.0","value": "Plants"},{"id": "8b7ae752-daca-4032-9fca-973ad711ab78","modelName": "GMOverriddenProperty","objectId": "13f642a5-3807-4b09-91a4-a15a954e3cc4","propertyId": "4dde7510-932e-4b89-9505-1cef9bfc365f","mvc": "1.0","value": "obj_plant"},{"id": "ad6551e4-66bc-41a5-867e-fd94515df2b8","modelName": "GMOverriddenProperty","objectId": "13f642a5-3807-4b09-91a4-a15a954e3cc4","propertyId": "094d42a1-bbec-404d-b96f-a760c3422ce1","mvc": "1.0","value": "32"},{"id": "4e25016d-5208-4fe4-aab4-91302094a439","modelName": "GMOverriddenProperty","objectId": "13f642a5-3807-4b09-91a4-a15a954e3cc4","propertyId": "038c3e52-8ea4-4857-9eb7-a395511d1f5d","mvc": "1.0","value": "96"}],"rotation": 0,"scaleX": 3,"scaleY": 0.5,"mvc": "1.0","x": 32,"y": 32},
{"name": "inst_740A3800","id": "29fa24af-e7a8-48e5-b012-c280bd6847a4","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_740A3800","objId": "13f642a5-3807-4b09-91a4-a15a954e3cc4","properties": [{"id": "6258f990-d4b1-45a9-81fc-da09688f77c8","modelName": "GMOverriddenProperty","objectId": "13f642a5-3807-4b09-91a4-a15a954e3cc4","propertyId": "4e348254-5b3b-4017-998e-60a128b761a4","mvc": "1.0","value": "Animals"},{"id": "202e4388-17d3-494e-9a93-182bbffa4604","modelName": "GMOverriddenProperty","objectId": "13f642a5-3807-4b09-91a4-a15a954e3cc4","propertyId": "4dde7510-932e-4b89-9505-1cef9bfc365f","mvc": "1.0","value": "obj_animal"},{"id": "e759aa95-a211-483b-98c5-bf93369485f5","modelName": "GMOverriddenProperty","objectId": "13f642a5-3807-4b09-91a4-a15a954e3cc4","propertyId": "094d42a1-bbec-404d-b96f-a760c3422ce1","mvc": "1.0","value": "288"},{"id": "6febc16c-569d-4461-8fb4-3cb5d3e8b4d1","modelName": "GMOverriddenProperty","objectId": "13f642a5-3807-4b09-91a4-a15a954e3cc4","propertyId": "038c3e52-8ea4-4857-9eb7-a395511d1f5d","mvc": "1.0","value": "96"}],"rotation": 0,"scaleX": 3,"scaleY": 0.5,"mvc": "1.0","x": 288,"y": 32},
{"name": "inst_265E09F8","id": "c299eb6d-e276-42e4-a896-b28901767206","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_265E09F8","objId": "1435fab1-e0eb-4875-80dd-8d635bed7a43","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 0,"y": 0},
{"name": "inst_7934B668","id": "f86e9446-f8a1-4915-a8bd-c33034ac3e7d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7934B668","objId": "7eb8828d-a9ed-443b-8b48-c6659e18fd83","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 32,"y": 0},
{"name": "inst_62B92154","id": "43acdeee-eace-4071-827e-454e5ccc5c26","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_62B92154","objId": "50590373-bc8a-417d-a50d-5799cb3e699a","properties": null,"rotation": 0,"scaleX": 5.5,"scaleY": 2.5,"mvc": "1.0","x": 768,"y": 544},
{"name": "inst_2F25083A","id": "d0cb6951-9f93-4120-b016-bdcde0e93393","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2F25083A","objId": "be2c774a-b2ac-4a26-a4e8-dfc04368e2d6","properties": null,"rotation": 0,"scaleX": 3.5,"scaleY": 1.5,"mvc": "1.0","x": 816,"y": 80}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "3bedd6d6-418c-45e8-b681-bd84b11b7bee",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4278190080 },
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "f559855b-3bac-404c-8a22-8da1d043bbca",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "b3136769-a2bd-402f-9c08-f02d0bb75afb",
        "Height": 640,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 960
    },
    "mvc": "1.0",
    "views": [
{"id": "dd7a7e9d-ca6e-458e-9c0f-59c6817e1ca1","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "4fb52147-533d-42b7-a5aa-162fed17536c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "09f0410c-743a-4ca5-84dd-742f99cd2a0d","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "c81c7261-e59c-477c-b304-e1dfe8eb0878","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "b050ed09-92f1-435d-8a8d-465a67a3125c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "d92aace6-0aff-4b70-8ca2-575864febbd9","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "d2c0b241-e3df-4117-b32e-c4871b8b168e","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "4b337f92-2f0b-4e74-87ca-60bbcba55257","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "cbb14e1f-7696-4ea7-af4e-f2913db7b45b",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}
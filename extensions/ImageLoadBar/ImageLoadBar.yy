{
    "id": "ba2e722f-8f5b-445f-9e36-610e531eb1fd",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "ImageLoadBar",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2018-29-03 03:07:37",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "bb0bcce7-af3c-4cd9-9e87-cca0e5ccda97",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 9223372036854775807,
            "filename": "ImageLoadBar.js",
            "final": "",
            "functions": [
                {
                    "id": "65071fb7-85b2-4001-801f-8cc3627c1241",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 6,
                    "args": [
                        2,
                        2,
                        2,
                        2,
                        2,
                        2
                    ],
                    "externalName": "ImageLoadBar_hook",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "ImageLoadBar_hook",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 5,
            "order": [
                
            ],
            "origname": "extensions\\ImageLoadBar.js",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "ACBD3CFF4E539AD869A0E8E3B4B022DD",
    "sourcedir": "",
    "version": "1.0.0"
}
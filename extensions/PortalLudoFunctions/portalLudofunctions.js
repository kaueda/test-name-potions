function update_ranking(ranking_key, score, secure){
	try {
		if(secure){
			old = false;
		} else {
			old = true;
		}
		parent.submitScore(ranking_key, score, old);
	} catch (e) {
		console.log(e.message);
		console.log("Parameters received are: RankingKey:", ranking_key, "Score:", score, "Secure", secure);
		console.log("Sending RankingKey, Score and Secure (the latter as:", old ? "true)." : "false).");
	}
}

function get_ssub_key(score, data){
	try {
		parent.requireKey(score, data, function(hh, ed){
			gml_Script_gmcallback_submitCallback(null, null, score, hh, ed);
		});
	} catch (e) {
		console.log(e.message);
		console.log("Parameters received are: Score:", score, "Data", data);
	}
}

function get_user_metadata(){
	try {
		return parent.getUser();
	} catch (e) {
		console.log(e.message);
		console.log("No parameters were sent or received.");
		return -1;
	}
}
{
    "id": "ddd5b0e3-bf59-4bb7-bd85-7b78a0a8e964",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "PortalLudoFunctions",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": true,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2018-24-03 03:07:07",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "75371e1e-f25b-405a-8162-2a268de59bff",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 123146358329582,
            "filename": "portalLudofunctions.js",
            "final": "",
            "functions": [
                {
                    "id": "5cba5630-e159-4db0-810c-39457384cd55",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 3,
                    "args": [
                        1,
                        2,
                        2
                    ],
                    "externalName": "update_ranking",
                    "help": "update_ranking_table(table_id, value, secure)",
                    "hidden": false,
                    "kind": 11,
                    "name": "update_ranking_table",
                    "returnType": 2
                },
                {
                    "id": "a60113f5-07a3-44b8-b683-bf6851bc4ac0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "get_user_metadata",
                    "help": "get_user_metadata()",
                    "hidden": false,
                    "kind": 11,
                    "name": "get_user_metadata",
                    "returnType": 1
                },
                {
                    "id": "b6cc7a71-338e-4530-80eb-637f4e2d643b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 2,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "get_ssub_key",
                    "help": "get_scoresub_key(score, data)",
                    "hidden": false,
                    "kind": 11,
                    "name": "get_scoresub_key",
                    "returnType": 1
                }
            ],
            "init": "",
            "kind": 5,
            "order": [
                
            ],
            "origname": "extensions\\portalLudofunctions.js",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": true,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "ACBD3CFF4E539AD869A0E8E3B4B022DD",
    "sourcedir": "",
    "version": "5.0.0"
}
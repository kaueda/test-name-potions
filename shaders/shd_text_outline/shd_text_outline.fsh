//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform vec2 size;
uniform float thick;
uniform vec4 iColor;
uniform vec4 oColor;
uniform float accuracy;
uniform float tol;
uniform vec4 uvs;

const float rad_circle = 6.28319;

void main()
{
    vec4 currColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	vec4 datPixel;
	vec2 check_pos;
    bool inkMe = true;
 
    for(float i = 1.0; i <= thick; i++){
        for(float d = 0.0; d < rad_circle; d += rad_circle/accuracy){
			check_pos = v_vTexcoord + i * vec2(cos(d) * size.x, -sin(d) * size.y);
            datPixel =  v_vColour * texture2D(gm_BaseTexture, check_pos);
            
			if (datPixel.a <= 0.0 || currColor.a <= 0.0){
                inkMe = false;
                break;
            }
        }
		if (!inkMe) break;
    }
    
    if(inkMe){ 
		gl_FragColor = iColor;
	} else {
		gl_FragColor.rgb = oColor.rgb;
		if(currColor.a > 0.0){
			gl_FragColor.a = oColor.a;
		} else {
			gl_FragColor.a = currColor.a;
		}
	}
}

//bool out_bound;
//out_bound = check_pos.x < uvs.r || check_pos.y < uvs.g || check_pos.x > uvs.b || check_pos.y > uvs.a;
//if (datPixel.a > tol && currColor.a <= tol && !out_bound){
//) && !out_bound

/*
//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform vec2 size;
uniform float thick;
uniform vec4 oColor;
uniform float accuracy;
uniform float tol;
uniform vec4 uvs;

const float rad_circle = 6.28319;

void main()
{
    vec4 currColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	vec4 datPixel;
	vec2 check_pos;
    bool inkMe = false;
 
    for(float i = 1.0; i <= thick; i++){
        for(float d = 0.0; d < rad_circle; d += rad_circle/accuracy){
			check_pos = v_vTexcoord + i * vec2(cos(d) * size.x, -sin(d) * size.y);
            datPixel =  v_vColour * texture2D(gm_BaseTexture, check_pos);
            
			if (currColor.a <= tol && datPixel.a > tol){
                inkMe = true;
                break;
            }
        }
		if (inkMe) break;
    }
    
    if(inkMe){
		gl_FragColor = oColor;
		//if(currColor.a > 0.0){
		//	gl_FragColor.a = oColor.a;
		//} else {
		//	gl_FragColor.a = currColor.a;
		//}
	} else {
		gl_FragColor = currColor;
		if(currColor.a >= tol){
			gl_FragColor.a = currColor.a;
		} else {
			gl_FragColor.a = 0.0;
		}
	}
}
*/
//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform vec4 blend_color; //The color to blend
uniform int blend_type; //The type of blend to be used

void main()
{
    vec4 rcolor = v_vColour * texture2D(gm_BaseTexture, v_vTexcoord);
	
	if(blend_type == 0){ //Simple Blinking Color Blend
		rcolor.r += (blend_color.r - rcolor.r) * blend_color.a;
		rcolor.g += (blend_color.g - rcolor.g) * blend_color.a;
		rcolor.b += (blend_color.b - rcolor.b) * blend_color.a;

		gl_FragColor = rcolor;
	} else if(blend_type == 1){ //Multiply - a * b
		rcolor.r = blend_color.r * rcolor.r;
		rcolor.g = blend_color.g * rcolor.g;
		rcolor.b = blend_color.b * rcolor.b;

		gl_FragColor = rcolor;
	} else if(blend_type == 2){ //Screen * 1 - (1 - a) * (1 - b)
		rcolor.r = 1.0 - ((1.0 - blend_color.r) * (1.0 - rcolor.r));
		rcolor.g = 1.0 - ((1.0 - blend_color.g) * (1.0 - rcolor.g));
		rcolor.b = 1.0 - ((1.0 - blend_color.b) * (1.0 - rcolor.b));

		gl_FragColor = rcolor;
	} else if(blend_type == 3){ //Darken - min(a, b)
		rcolor.r = min(blend_color.r, rcolor.r);
		rcolor.g = min(blend_color.g, rcolor.g);
		rcolor.b = min(blend_color.b, rcolor.b);

		gl_FragColor = rcolor;
	} else if(blend_type == 4){ //Lighten - max(a, b)
		rcolor.r = max(blend_color.r, rcolor.r);
		rcolor.g = max(blend_color.g, rcolor.g);
		rcolor.b = max(blend_color.b, rcolor.b);

		gl_FragColor = rcolor;
	} else if(blend_type == 5){ //Difference - abs(a - b)
		rcolor.r = abs(rcolor.r - blend_color.r);
		rcolor.g = abs(rcolor.g - blend_color.g);
		rcolor.b = abs(rcolor.b - blend_color.b);

		gl_FragColor = rcolor;
	} else if(blend_type == 6){ //Negation - 1 - abs(1 - a - b)
		rcolor.r = 1.0 - abs(1.0 - rcolor.r - blend_color.r);
		rcolor.g = 1.0 - abs(1.0 - rcolor.g - blend_color.g);
		rcolor.b = 1.0 - abs(1.0 - rcolor.b - blend_color.b);

		gl_FragColor = rcolor;
	} else if(blend_type == 7){ //Exclusion - a + b - 2 * a * b
		rcolor.r = rcolor.r + blend_color.r - (2.0 * rcolor.r * blend_color.r);
		rcolor.g = rcolor.g + blend_color.g - (2.0 * rcolor.g * blend_color.g);
		rcolor.b = rcolor.b + blend_color.b - (2.0 * rcolor.b * blend_color.b);

		gl_FragColor = rcolor;
	} else if(blend_type == 8){ //Overlay - a < .5 ? (2 * a * b) ){ (1 - 2 * (1 - a) * (1 - b))
		if (rcolor.r > 0.5){
		    rcolor.r = 1.0 - (2.0 * (1.0 - rcolor.r) * (1.0 - blend_color.r));
		} else {   
		    rcolor.r = 2.0 * rcolor.r * blend_color.r;
		}

		if (rcolor.g > 0.5) {
		    rcolor.g = 1.0 - (2.0 * (1.0 - rcolor.g) * (1.0 - blend_color.g));
		} else {   
		    rcolor.g = 2.0 * rcolor.g * blend_color.g;
		}

		if (rcolor.b > 0.5) {
		    rcolor.b = 1.0 - (2.0 * (1.0 - rcolor.b) * (1.0 - blend_color.b));
		} else {   
		    rcolor.b = 2.0 * rcolor.b * blend_color.b;
		}
 
		gl_FragColor = rcolor;
	} else if(blend_type == 9){ //Hard Light - b < .5 ? (2 * a * b) ){ (1 - 2 * (1 - a) * (1 - b))
		if (blend_color.r > 0.5){
		    rcolor.r = 1.0 - (2.0 * (1.0 - rcolor.r) * (1.0 - blend_color.r));
		} else {   
		    rcolor.r = 2.0 * rcolor.r * blend_color.r;
		}

		if (blend_color.g > 0.5) {
		    rcolor.g = 1.0 - (2.0 * (1.0 - rcolor.g) * (1.0 - blend_color.g));
		} else {   
		    rcolor.g = 2.0 * rcolor.g * blend_color.g;
		}

		if (blend_color.b > 0.5) {
		    rcolor.b = 1.0 - (2.0 * (1.0 - rcolor.b) * (1.0 - blend_color.b));
		} else {   
		    rcolor.b = 2.0 * rcolor.b * blend_color.b;
		}
 
		gl_FragColor = rcolor;
	} else if(blend_type == 10){ //Soft Light - b < .5 ? (2 * a * b + a * a * (1 - 2 * b)) ){ (sqrt(a) * (2 * b - 1) + (2 * a) * (1 - b))
		if (blend_color.r > 0.5){
		    rcolor.r = (2.0 * rcolor.r * blend_color.r) + (rcolor.r * rcolor.r * (1.0 - (2.0 * blend_color.r)));
		} else {   
		    rcolor.r = (sqrt(rcolor.r) * ((2.0 * blend_color.r) - 1.0)) + ((2.0 * rcolor.r) * (1.0 - blend_color.r));
		}

		if (blend_color.g > 0.5) {
		    rcolor.g = (2.0 * rcolor.g * blend_color.g) + (rcolor.g * rcolor.g * (1.0 - (2.0 * blend_color.g)));
		} else {   
		    rcolor.g = (sqrt(rcolor.g) * ((2.0 * blend_color.g) - 1.0)) + ((2.0 * rcolor.g) * (1.0 - blend_color.g));
		}

		if (blend_color.b > 0.5) {
		    rcolor.b = (2.0 * rcolor.b * blend_color.b) + (rcolor.b * rcolor.b * (1.0 - (2.0 * blend_color.b)));
		} else {   
		    rcolor.b = (sqrt(rcolor.b) * ((2.0 * blend_color.b) - 1.0)) + ((2.0 * rcolor.b) * (1.0 - blend_color.b));
		}
 
		gl_FragColor = rcolor;
	} else if(blend_type == 11){ //Dodge - a / (1 - b)
		rcolor.r = rcolor.r / (1.0 - blend_color.r);
		rcolor.g = rcolor.g / (1.0 - blend_color.g);
		rcolor.b = rcolor.b / (1.0 - blend_color.b);

		gl_FragColor = rcolor;
	} else if(blend_type == 12){ //Burn - 1 - (1 - a) / b
		rcolor.r = 1.0 - ((1.0 - rcolor.r) / blend_color.r);
		rcolor.g = 1.0 - ((1.0 - rcolor.g) / blend_color.g);
		rcolor.b = 1.0 - ((1.0 - rcolor.b) / blend_color.b);

		gl_FragColor = rcolor;
	}
}
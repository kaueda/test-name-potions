/// @function array_swap(array, p1, p2)
/// @description Swap element on position p1 with element on position p2, returning the new array
/// @param {array} array The array to swap from
/// @param {integer} p1 The position one you would like to swap
/// @param {integer} p2 The position two you would like to swap

var newArray = array_copyAll(argument[0]);
array_swap_in_place(newArray, argument[1], argument[2]);
return newArray;
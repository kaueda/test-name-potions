/// @description Trigger an event when an animation ends
/// @param {object} animation_controller Controller performing the animation
/// @param {object} object The object that has the event to be triggered.
/// @param {event_type} [event_type=ev_other] event_type The type of the event to be triggered.
/// @param {event_number} [event_number=ev_user1] event_number The number of the event to be triggered.

var ctrl, obj, ev_type, ev_no;

ctrl = argument[0];
obj = argument[1];

if(argument_count > 2){
    ev_type = argument[2];
    ev_no = argument[3];
}
else{
    ev_type = ev_other;
    ev_no = ev_user1;
}

if(instance_exists(ctrl)){
    return anima_add_timed_event(ctrl, 1, false, obj, ev_type, ev_no);
}
else{
    return false;
}

/// @function sm_stop_Music(musicIndex, fadeDuration[0.3])
/// @description Stops a Musi, fading out itself.
/// @param {sound} musicIndex The music you want to stop playing.

var music = argument[0];
var fadeT = argument_count > 1 ? argument[1] : 0.3;

var ctrl = obj_soundManager;
with(ctrl){
	if(music != noone && audio_is_playing(music)){
	    audio_sound_gain(music, 0, fadeT*1000);
		
		if(playingIntro){
			realMusic = noone;
			realMusicFade = noone;
			realMusicLoop = noone;
			realMusicIntro = noone;
			
			playingIntro = false;
			introLength = -1;
			introGain = 0;
		}
		onStop = music;
		
	    alarm[0] = room_speed * fadeT; ///Stops and Resets to noone;
	}
}
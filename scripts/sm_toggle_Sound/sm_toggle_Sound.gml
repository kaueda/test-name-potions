/// @function sm_mute_Sound()
/// @description Mutes/Unmutes the game's sounds

var ctrl = obj_soundManager;
with(ctrl){
	soundEnabled = !soundEnabled;
}
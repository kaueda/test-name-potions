/// @function array_swap_in_place(array, p1, p2)
/// @description Swap element on position p1 with element on position p2, in place
/// @param {array} array The array to swap from
/// @param {integer} p1 The position one you would like to swap
/// @param {integer} p2 The position two you would like to swap

var array = argument[0];
var p1 = argument[1];
var p2 = argument[2];
var tmp;

if(p1 != p2){
	tmp = array[@ p1];
	array[@ p1] = array[@ p2];
	array[@ p2] = tmp;
}
/// @function save_ini_values(iniFileArray, key_value_Array)
/// @description Saves the data in the array "key_value_Array" (in the format [key, value]), ignoring everything and forcefully saving the value.
/// @param {array} iniFileArray The array created when save_init was called or an array of the form [iniPath, userSection]
/// @param {array} key_value_Array An array of key/value pair (also arrays) of the fields to be saved at the passed section

// Example: save_ini_values(global.iniData, [["points", 50], ["levels", 2]]);

var iniFileArray = argument[0];
var kvdArray = argument[1];
var tmpArray;

ini_open(iniFileArray[0]);

for(var i=0; i<array_length_1d(kvdArray); i++){
	tmpArray = kvdArray[i];
	
	if(is_string(tmpArray[1])){
		ini_write_string(iniFileArray[1], tmpArray[0], tmpArray[1]);
	} else {
		ini_write_real(iniFileArray[1], tmpArray[0], tmpArray[1]);
	}
}

ini_close();
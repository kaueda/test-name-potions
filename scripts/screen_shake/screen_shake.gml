/// @function screen_shake(camera, max_shake_distance, shake_time)
/// @description Treme a camera até um maximo de pixels por um tempo
/// @param {id} camera A camera a ser tremida
/// @param {real} max_shake_distance O máximo que a camera pode ser deslocada durante o tremer
/// @param {real} shake_time O tempo que levará a tremedeira em segundos

var screen_shake_sc = instance_create_depth(x, y, depth, obj_screen_shake);

screen_shake_sc.camera = argument[0];
screen_shake_sc.max_shake_distance = argument[1];
screen_shake_sc.shake_time = argument[2] * room_speed;
screen_shake_sc.shake_dec_factor = screen_shake_sc.max_shake_distance / screen_shake_sc.shake_time;
screen_shake_sc.can_shake = true;
screen_shake_sc.initial_x = camera_get_view_x(screen_shake_sc.camera);
screen_shake_sc.initial_y = camera_get_view_y(screen_shake_sc.camera);

return screen_shake_sc;
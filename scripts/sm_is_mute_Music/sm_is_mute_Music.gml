/// @function sm_is_mute_Music()
/// @description Checks if the game's musics are muted

var ctrl = obj_soundManager;
var ret = false; // We need to use With for the SD game's cases

with(ctrl){
	ret = musicVolume == 0;
}
return ret;
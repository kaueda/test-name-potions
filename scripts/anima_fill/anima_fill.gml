/// @description Fill screen/camera with color
/// @param {color} fill_color
/// @param {real} alpha
/// @param {boolean} persistent
/// @param {boolean} [is_draw_gui=false] is_draw gui
var color, alpha, is_draw_gui, is_persistent, camera;
var ret;

color = argument[0];
alpha = argument[1];
is_persistent = argument[2];
is_draw_gui = (argument_count >= 4) && argument[3];
//camera = (argument_count == 5) && argument[3];

ret = instance_create_depth(0, 0, depth, obj_anima_fill_camera);

//if(camera_
ret.camera = camera_get_default();
ret.color = color;
ret.is_draw_gui = is_draw_gui;
ret.image_alpha = alpha;
ret.persistent = is_persistent;
ret.can_draw = true;
if(alpha == 0){
    ret.visible = false;
}

return ret;

/// @function slide_view_port(camera, dx, dy, time, is_by, pers, slide_obj , easing_script)
/// @description Slide a camera, por um tempo e com um determinado easing
/// @param camera A camera que deverá ser slided
/// @param dx O deslocamento no eixo x da camera
/// @param dy O deslocamento no eixo y da camera
/// @param time O tempo que levara o deslocamento da camera
/// @param [is_by=true] Se o deslocamento é relativo (by) ou absoluto (not by)
/// @param [pers=false] Se o objeto de slide é continuará existindo após o movimento completo do slide
/// @param [slide_obj] Um objeto de slide, caso algum já exista.
/// @param [easing_script] Um easing a ser utilizado no slide

var slider, _is_by, _pers, _easing;

_is_by = argument_count-1 >= 4 ? argument[4] : false;
_pers = argument_count-1 >= 5 ? argument[5] : false;
slider = argument_count-1 >= 6 ? argument[6] : noone;
_easing = argument_count-1 >= 7 ? argument[7] : ease_linear;

if(slider == noone || !instance_exists(slider)){
    slider = instance_create_depth( 0, 0, depth, obj_viewport_slider);
}	

slider.camera = argument[0];
slider.dx = argument[1];
slider.dy = argument[2];
slider.time = argument[3]*room_speed;

with(slider){
	initial_x = camera_get_view_x(camera);
	initial_y = camera_get_view_y(camera);
	easing_script = _easing;
	persistent = _pers;
	is_by = _is_by;
	dest_x = 0;
    dest_y = 0;
    
    if(is_by){
        dest_x = initial_x + dx;
		dest_y = initial_y + dy;
    } else {
        dest_x = dx;
        dest_y = dy;        
    }
    can_slide = true;
}

return slider;
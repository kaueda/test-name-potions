/// @description Slide an object to the given coordinates.
/// @param target_object The object to be slide.
/// @param target_x The object's x position after the slide.
/// @param target_y The object's y position after the slide.
/// @param duration_in_secs The duration in seconds of the slide.       
/// @param loop If set to true, the animation will loop.
/// @param start_animation_on_stop If set to true the animation must be manually started.
/// @param [easing_script] The easing script used to perform the slide.

var target_obj, final_x, final_y, duration, loop, destroy_after_use, easing_script, start_on_stop;
var ret;

target_obj = argument[0];
final_x = argument[1];
final_y = argument[2];
duration = argument[3] * 1000000;
loop = argument[4];
destroy_after_use = !loop;
start_on_stop = argument[5];
if(argument_count == 7){
    easing_script = argument[6];   
}
else{
    easing_script = anima_ease_linear;
}

ret = instance_create_depth(0, 0, 0, obj_anima_slide);

ret.target_obj = target_obj;
ret.no_of_values = 2;
ret.final_value[0] = final_x;
ret.final_value[1] = final_y;
ret.easing = easing_script;
ret.duration = duration;
ret.loop = loop;
ret.destroy_after_use = destroy_after_use;
ret.is_by = false;

if(!start_on_stop){
    ret.can_animate = true;
}

return ret;

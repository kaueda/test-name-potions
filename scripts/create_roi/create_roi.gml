/// @function create_roi(x, y, width, height)
/// @description Creates a Region Of Interest at depth 0 on a (x,y) position with (width,height) size.
/// @param {real} x The x position of the Roi
/// @param {real} y The y position of the Roi
/// @param {real} width The width position of the Roi
/// @param {real} height The height position of the Roi

var ret = instance_create_depth(argument[0], argument[1], depth, obj_roi);

ret.image_xscale = argument[2] / ret.sprite_width;
ret.image_yscale = argument[3] / ret.sprite_height;

return ret;
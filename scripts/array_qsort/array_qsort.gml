/// @function array_qsort(array)
/// @description Sort the array using Quick Sort, returning the new array
/// @param {array} array The array to be qsorted

var newArray = array_copyAll(argument[0]);
array_qsort_in_place(newArray);

return newArray;

/// @function convert_digit_hex_dec(hex_digit)
/// @description Converts a Hex digit to a number
/// @param {char} hex_digit The hexa char to convert

var convert_digit_hex_dec_input = string_lower(argument[0]);

switch(convert_digit_hex_dec_input){
	case "a":
		return 10;
	break;
	
	case "b":
		return 11;
	break;
	
	case "c":
		return 12;
	break;
	
	case "d":
		return 13;
	break;
	
	case "e":
		return 14;
	break;
	
	case "f":
		return 15;
	break;
	
	default:
		return real(convert_digit_hex_dec_input);
	break;
}
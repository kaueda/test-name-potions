/// @function save_ini_read(iniFileArray, key_default_type_Array)
/// @description Loads the data defined by "key_default_type_Array" (in the format [key, default, type]), checking the default value, if it doesn't exist.
/// It also returns an array in the same order, with the loaded values
/// @param {array} iniFileArray The array created when save_init was called or an array of the form [iniPath, userSection]
/// @param {array} key_default_type_Array An array of key/default/type pair (also arrays) of the fields to be read from the passed section. Types can be "s" (string) or "r" (real) 

// Example: var read = save_ini_read(global.iniData, [["levels", 0, "r"], ["badges", 0, "r"]]);

var iniFileArray = argument[0];
var kdtArray = argument[1];
var tmpArray;
var ret = [];

ini_open(iniFileArray[0]);

for(var i=0; i<array_length_1d(kdtArray); i++){
	tmpArray = kdtArray[i];
	
	if(tmpArray[2] == "s"){
		ret[i] = ini_read_string(iniFileArray[1], tmpArray[0], tmpArray[1]);
	} else { //tmpArray[2] == "r"
		ret[i] = ini_read_real(iniFileArray[1], tmpArray[0], tmpArray[1]);
	}
}

ini_close();

return ret;
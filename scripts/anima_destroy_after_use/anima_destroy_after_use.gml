/// @description Sets if the animation controller is goint to be destroyed after it ends
/// @param {object} animation_controller Animation controller
/// @param {boolean} destroy_after_animation_end Sets if the animation controller is goint to be destroyed after it ends

var animations_controller, destroy_after_use;

animations_controller = argument[0];
destroy_after_use = argument[1];
if(instance_exists(animations_controller)){
    animations_controller.destroy_after_use = destroy_after_use;
    return true;
}
else{
    return false;
}

/// @function ma(element0, element1, element2, ...)
/// @description Creates an array with all elements (max 16)
/// @param {anything} element0 The first element
/// @param {anything} [element1] The second element
/// @param {anything} [element2] The third element
/// @param {anything} [...] Other elements

var ret = 0;
for(var i = argument_count-1; i >= 0; i--){
	ret[i] = argument[i];
}
return ret;
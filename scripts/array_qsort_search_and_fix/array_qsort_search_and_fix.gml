/// @function array_qsort_search_and_fix(array, pivot, left, right)
/// @description Searches on a part of the array and swaps according to the pivot, returns Left-1
/// @param

var array = argument[0];
var pivot = argument[1];
var left = argument[2];
var right = argument[3];

var moveRight, moveLeft;

moveLeft = true;
moveRight = true;
while(left <= right){ //Leaves if at any moment left > right
	if(moveLeft){ //If moving left and smaller, go to next, else stop cursor
		if(array[left] < array[pivot]){
			left++;
		} else {
			moveLeft = false;
		}
	}
			
	if(moveRight){ //If moving right and bigger (or equal), go to next, else stop cursor
		if(array[right] >= array[pivot]){
			right--;
		} else {
			moveRight = false;
		}
	}
			
	if(!moveLeft && !moveRight){ //Cant move any side, swap them
		moveLeft = true;
		moveRight = true;
		array_swap_in_place(array, left, right);
	}
}

return left-1;
/// @function anchor_to_screen(object, follow_cam_ratio, camera)
/// @description Anchors an obejct to the screen and returns the anchor object
/// @param {object} object The object to be anchored
/// @param {bool} [follow_cam_ratio=true] If the anchored object should follow the ratio of the camera, if it get bigger or smaller later
/// @param {id} [camera=default_camera] The camera to anchor to

var ret = noone;

var _target = argument[0];
var follow_ratio = argument_count-1 >= 1 ? argument[1] : true;
var cam = argument_count-1 >= 2 ? argument[2] : camera_get_default();

/*if(argument_count == 3){
	follow_ratio = argument[1];
    cam = argument[2];
}
else if(argument_count == 2){
    follow_ratio = argument[1];
}*/

ret = instance_create_depth(0, 0, depth, obj_anchor_to_screen);
with(ret){
	target = _target;
	follow_cam_ratio = follow_ratio;
	camera = cam;
	initial_x = target.x;
	initial_y = target.y;
}

return ret;
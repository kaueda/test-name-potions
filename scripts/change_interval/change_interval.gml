/// @function change_interval(value, src_max, src_min, dest_max, dest_min, script)
/// @description Troca o intervalo de um valor usando um easing, se passado
/// @param {real} value O valor a ser colocado dentro do intervalo
/// @param {real} src_max O valor máximo do valor real
/// @param {real} src_min O valor mínimo do valor real
/// @param {real} dest_max O valor máximo do novo valor
/// @param {real} dest_min O valor mínimo do novo valor
/// @param {resource} [script=linear] Script de Easing que se deseja usar
var script = argument_count-1 >= 5 ? argument[5] : ease_linear;

return script_execute(script, (argument[0] - argument[2])/(argument[1] - argument[2]), argument[4], argument[3] - argument[4], 1);

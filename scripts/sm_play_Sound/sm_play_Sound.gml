/// @function sm_play_Sound(soundIndex)
/// @description Plays a sound.
/// @param {sound} soundIndex The sound you want to play

var soundIndex = argument[0];

var ctrl = obj_soundManager;
with(ctrl){
    if(soundEnabled){
        audio_play_sound(soundIndex, 2, false);
    }
}
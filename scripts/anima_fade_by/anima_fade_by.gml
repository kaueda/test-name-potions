/// @description Fade object to specified alpha.
/// @param {object} target_object The object to be faded.
/// @param {real} alpha_variation Alpha variation
/// @param {real} duration_in_secs The duration in seconds of the fade.       
/// @param {boolean} loop If set to true, the animation will loop.
/// @param {boolean} start_animation_on_stop If set to true the animation must be manually started.
/// @param {script} [easing_script=anima_ease_linear] The easing script used to perform the slide.

var target_obj, alpha_displacement, duration, loop, destroy_after_use, easing_script, start_on_stop;
var anima_slide_to_ret;

target_obj = argument[0];
alpha_displacement = argument[1];
duration = argument[2] * 1000000;
loop = argument[3];
destroy_after_use = !loop;
start_on_stop = argument[4];
if(argument_count == 6){
    easing_script = argument[5];   
}
else{
    easing_script = anima_ease_linear;
}

anima_slide_to_ret = instance_create_depth(0, 0, 0, obj_anima_fade);

anima_slide_to_ret.target_obj = target_obj;
anima_slide_to_ret.no_of_values = 1;
anima_slide_to_ret.value_difference[0] = alpha_displacement;
anima_slide_to_ret.easing = easing_script;
anima_slide_to_ret.duration = duration;
anima_slide_to_ret.loop = loop;
anima_slide_to_ret.destroy_after_use = destroy_after_use;
anima_slide_to_ret.is_by = true;

if(!start_on_stop){
    anima_slide_to_ret.can_animate = true;
}

return anima_slide_to_ret;

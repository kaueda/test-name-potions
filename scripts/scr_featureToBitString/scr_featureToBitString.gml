/// @function scr_featureToBitString(feature, maxbits)
/// @description Returns a string with the current bit state of the feature
/// @param feature The feature you want the bit_string from
/// @param [maxbits=ideal] The maximum number of bits you want to check/print

/// Usage: var string = scr_featureToBitString(currentFeature); OR scr_featureToBitString((1 << ON_GROUND) & (1 << STILL));
var value = argument[0];
var bitsize = argument_count-1 >= 1 ? argument[1] : (value > 0 ? floor(log2(value))+1 : 1);
var ret = "";

for(var i=0; i < bitsize; i++){
	ret = (value & 1 ? "1" : "0") + ret;
	value = value >> 1;
}

return ret;
/*
for(var i=1; i<10; i++){
	show_debug_message(string(i) + " <-> " + string(floor(log2(i))+1));
}
*/

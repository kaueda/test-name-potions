/// @description Add a pivot point.
/// @param animation
/// @param x
/// @param y
/// @param coords_are_absolute

var _animation, _x, _y, _coords_are_absolute;

_animation = argument[0];
_x = argument[1];
_y = argument[2];
_coords_are_absolute = argument[3];

if(_animation.object_index == obj_anima_rotate){
	_animation.has_point_of_rotation = true;
	_animation.rotation_point_x = _x;
	_animation.rotation_point_y = _y;
	_animation.pivot_coords_are_absolute = _coords_are_absolute;
}
else if(_animation.object_index == obj_anima_scale){
	var _p_w_b_x, _p_w_b_y, _p_w_a_x, _p_w_a_y, _p_r_b_x, _p_r_b_y, _p_r_a_x, _p_r_a_y;
	
	if(_coords_are_absolute){
		_p_w_b_x = _x;
		_p_w_b_y = _y;
		_p_r_b_x = _x - _animation.target_obj.x;
		_p_r_b_y = _y - _animation.target_obj.y;
	}
	else{
		_p_w_b_x = _animation.target_obj.x + _x;
		_p_w_b_y = _animation.target_obj.y + _y;
		_p_r_b_x = _x;
		_p_r_b_y = _y;
	}

	if(_animation.is_by){
		_p_r_a_x = _p_r_b_x * _animation.value_difference[0];
		_p_r_a_y = _p_r_b_y * _animation.value_difference[1];
	}
	else{
		_p_r_a_x = _p_r_b_x * (_animation.final_value[0] / _animation.target_obj.image_xscale);
		_p_r_a_y = _p_r_b_y * (_animation.final_value[1] / _animation.target_obj.image_yscale);
	}

	_p_w_a_x = _animation.target_obj.x + _p_r_a_x;
	_p_w_a_y = _animation.target_obj.y + _p_r_a_y;

	_animation.initial_value[2] = _animation.target_obj.x;
	_animation.initial_value[3] = _animation.target_obj.y;
	_animation.final_value[2] = _animation.target_obj.x + (_p_w_b_x - _p_w_a_x);
	_animation.final_value[3] = _animation.target_obj.y + (_p_w_b_y - _p_w_a_y);
	_animation.current_value[2] = _animation.initial_value[2];
	_animation.current_value[3] = _animation.initial_value[3];
	_animation.value_difference[2] = _animation.final_value[2] - _animation.initial_value[2];
	_animation.value_difference[3] = _animation.final_value[3] - _animation.initial_value[3];
	_animation.has_pivot = true;
	_animation.no_of_values = 4;
}
else if(_animation.object_index == obj_anima_slide){
	// Pivot will act as an origin to the system	
	//if(_coords_are_absolute){
	//	_animation.current_value[2] = _x;
	//	_animation.current_value[3] = _y;
	//}
	//else{
	//	_animation.current_value[2] = _x - _animation.target_obj.x;
	//	_animation.current_value[3] = _y - _animation.target_obj.y;
	//}
	//_animation.has_pivot = true;
	show_debug_message("Slide not supported yet");
}
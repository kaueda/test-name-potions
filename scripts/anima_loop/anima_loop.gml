/// @description Set if the animation loops or not
/// @param {object} animation_controller Animation controller
/// @param {boolean} loop
var anima_loop_animation_controller, enable_loop;

anima_loop_animation_controller = argument[0];
enable_loop = argument[1];

if(instance_exists(anima_loop_animation_controller)){
	anima_loop_animation_controller.loop = enable_loop;
    return true;
}
else{
    return false;
}

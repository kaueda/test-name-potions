/// @function trim_white_spaces(string, type_of_trim, split_interval)
/// @description Removes (trims) white spaces in a certain part of the string
/// @param {string} string The string to trim from
/// @param {macro} type_of_trim Choose between (0:BEGIN, 1:END, 2:BOTH)
/// @param {array} split_interval An array representing an splitted interval of the string to look at [begin, end]

var input_string = argument[0];
var ammount_to_delete;
var i;
var starting_trim_position;
var read_char;
var split;

if(argument_count == 3){
	split = argument[2];
    var string_part_a = string_delete(input_string, split[0], string_length(input_string) - split[0] + 1);    
    var string_part_b = string_delete(input_string, 1, split[1] - 1);
    input_string = string_delete(input_string, split[1], string_length(input_string) - split[1] + 1);
    input_string = string_delete(input_string, 1, split[0] - 1);    
}

var return_array = 0;
// 0: Trimmed string
// 1: Ammount trimmed at the beginning
// 2: Ammount trimmed at the end

if ((argument[1] == 0) || (argument[1] == 2)){
    starting_trim_position = 1;
    ammount_to_delete = 0;
    for(i = 1; i < string_length( input_string ); i++){  
        read_char = string_char_at(input_string, i);
        if(read_char != " "){
            break;
        }
        else{
            ammount_to_delete++;
        }
    }
    return_array[1] = ammount_to_delete;
    input_string = string_delete(input_string, starting_trim_position, ammount_to_delete);    
}
if ((argument[1] == 1) || (argument[1] == 2)){
    starting_trim_position = 1;
    ammount_to_delete = 0;
    for(i = string_length( input_string ); i > 0 ; i--){  
        read_char = string_char_at(input_string, i);
        if(read_char == " "){
            starting_trim_position = i;
            ammount_to_delete++;
        }
        else{
            break;
        }
    }
    return_array[2] = ammount_to_delete;
    input_string = string_delete(input_string, starting_trim_position, ammount_to_delete);
}

if(argument_count == 4){
    input_string = string_part_a + input_string + string_part_b;
}

return_array[0] = input_string;

return return_array;

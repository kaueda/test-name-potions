/// @function make_colour_hex(hex_value)
/// @description Creates a color from an Hex value
/// @param {string} hex_value The string containing the Hexadecimal of the desired colour

var hex_char_pos = 1;
var hex_length = string_length(argument[0]);

if(hex_length != 6 && hex_length != 7){
    return noone;
}
if(hex_length == 7){ ///Skips #
    hex_char_pos++;
}

var char_hex_r = string_copy(argument[0], hex_char_pos, 2);
var char_hex_g = string_copy(argument[0], hex_char_pos + 2, 2);
var char_hex_b = string_copy(argument[0], hex_char_pos + 4, 2);

var char_dec_r = convert_digit_hex_dec(string_char_at(char_hex_r, 1)) * 16 + convert_digit_hex_dec(string_char_at(char_hex_r, 2));
var char_dec_g = convert_digit_hex_dec(string_char_at(char_hex_g, 1)) * 16 + convert_digit_hex_dec(string_char_at(char_hex_g, 2));
var char_dec_b = convert_digit_hex_dec(string_char_at(char_hex_b, 1)) * 16 + convert_digit_hex_dec(string_char_at(char_hex_b, 2));

return make_colour_rgb(char_dec_r, char_dec_g, char_dec_b);

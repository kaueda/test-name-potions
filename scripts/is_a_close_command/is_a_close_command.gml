/// @function is_a_close_command(string)
/// @description Checks if the passed char is or is not a close command
/// @param {char} string The char to check

var command_string = argument[0];
if(string_char_at(command_string, 1) == "/"){
    return true;
}
else{
    return false;
}

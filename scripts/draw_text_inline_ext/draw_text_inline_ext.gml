/// @function draw_text_inline_ext(x, y, text, font, sep, width, thickness, outColor, outAlpha, accuracy, tolerance)
/// @description Creates an outline over the text. This can only be used during a draw event!
/// @param {integer} x The x position to draw the text at
/// @param {integer} y The y position to draw the text at
/// @param {string} text The text to be draw
/// @param {font} font The font that will be used in the text draw
/// @param {integer} sep The separation of the text lines
/// @param {integer} width The width of the text box to be used
/// @param {integer} [thickness=1] The thickness of the outline in pixels
/// @param {color} [outColor=c_black] The color of the outline
/// @param {integer} [outAlpha=1] The alpha of the outline (independent of the already set alpha)
/// @param {integer} [accuracy=8] The precision of the pixel check. This number can be either from 360 to 4 (Degree by Degree or 90 Degree by 90 Degree)
/// @param {integer} [tolerance=0] The tolerance of alpha to be considered completely transparent, use only if needed

var xx = argument[0]; //X
var yy = argument[1]; //Y
var text = argument[2]; //Text
var font = argument[3]; //Font
var sep = argument[4]; //Sep
var width = argument[5]; //Width
var thickness = argument_count > 6 ? argument[6] : 1; //Thickness
var oColor = argument_count > 7 ? argument[7] : c_black; //outColor
var outalpha = argument_count > 8 ? argument[8] : 1; //outAlpha
var acc = argument_count > 9 ? argument[9] : 8; //Accuracy
var tol = argument_count > 10 ? argument[10] : 0; //Tolerance

var iColor = draw_get_color();
var inAlpha = draw_get_alpha();

var ext = sep != -1 && width != -1;
var tex = font_get_texture(font);
var uvs = font_get_uvs(font);
var w = texture_get_texel_width(tex);
var h = texture_get_texel_height(tex);

shader_set(shd_text_outline);
shader_set_uniform_f(uni_size, w, h);
shader_set_uniform_f(uni_uvs, uvs[0], uvs[1], uvs[2], uvs[3]);
shader_set_uniform_f(uni_thick, thickness);

shader_set_uniform_f(uni_icolor, color_get_red(iColor)/255, color_get_green(iColor)/255, color_get_blue(iColor)/255, inAlpha);
shader_set_uniform_f(uni_ocolor, color_get_red(oColor)/255, color_get_green(oColor)/255, color_get_blue(oColor)/255, outalpha);
shader_set_uniform_f(uni_acc, acc);
shader_set_uniform_f(uni_tol, tol);

draw_set_font(font);
if(ext){
	draw_text_ext(xx, yy, text, sep, width);
} else {
	draw_text(xx, yy, text);
}

shader_reset();
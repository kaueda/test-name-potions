/// @description ease_linear(time, start, change, duration)
/// @function ease_linear
/// @param time
/// @param start
/// @param change
/// @param duration

return argument[2] * argument[0] / argument[3] + argument[1];

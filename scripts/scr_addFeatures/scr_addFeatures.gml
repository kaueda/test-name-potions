/// @function scr_addFeatures(currentFeature, newFeatures)
/// @description Adds one or more features to the initial feature
/// @param currentFeature The initial feature
/// @param newFeatures The new feature (or coumpound) to be added

/// Usage: var initial = scr_addFeatures(0, (1 << STILL) | (1 << ON_GROUND));
return argument[0] | argument[1];

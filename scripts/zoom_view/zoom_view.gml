/// @function zoom_view(camera, final_width, final_height, zoom_duration, easing_script, zoom_out_obj)
/// @description Faz um zoom in ou out de uma camera, usando-se de um easing (não deleta o objeto retornado)
/// @param {id} camera A camera que recebera o zoom in ou out
/// @param {real} final_width O valor final de largura da camera passada
/// @param {real} final_height O valor final de altura da camera passada
/// @param {real} zoom_duration A duração de toda animação de zoom
/// @param {script} easing_script O script de easing usado para realizar a animação de zoom
/// @param {object} [zoom_out_obj] Caso já exista um objeto de zoom, você pode passa-lo aqui

var zoom_out_return = argument_count-1 >= 5 ? argument[5] : noone;

if(zoom_out_return == noone || !instance_exists(zoom_out_return) || zoom_out_return.object_index != obj_camera_zooming){
    zoom_out_return = instance_create_depth(0, 0, depth, obj_camera_zooming);
}

zoom_out_return.camera = argument[0];
zoom_out_return.final_w = argument[1];
zoom_out_return.final_h = argument[2];
zoom_out_return.zooming_duration = argument[3]*room_speed;
zoom_out_return.easing_script = argument[4];
zoom_out_return.initial_w = camera_get_view_width(zoom_out_return.camera);
zoom_out_return.initial_h = camera_get_view_height(zoom_out_return.camera);
zoom_out_return.zooming = true;
zoom_out_return.zooming_time_counter = 0;

return zoom_out_return;

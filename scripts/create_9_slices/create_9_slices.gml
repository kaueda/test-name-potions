/// @function create_9_slices(x, y, width, height, spr_array, animate_one_time)
/// @description Creates a 9 slice Object at (x,y) position with depth 0 and (width,height) size.
/// @param {real} x The starting x of the object
/// @param {real} y The starting y of the object
/// @param {real} width The width of the object
/// @param {real} height The height of the object
/// @param {asset} spr_array An array containing the sprites to be used
/// @param {boolean} [animate_one_time=false] If this object should animate only once then stop

var objx, objy, width, height, sprite_array, animate_just_one_time;
var ret_obj, single_sprite_mode;
objx = argument[0];
objy = argument[1];
width = argument[2];
height = argument[3];
sprite_array = argument[4];
single_sprite_mode = array_length_1d(sprite_array) == 1;
animate_just_one_time = argument_count-1 >= 5 ? argument[5] : false; /*if(argument_count > 5){ animate_just_one_time = ; }*/

ret_obj = instance_create_depth(objx, objy, 0, obj_9_slices);

with(ret_obj){
	self.single_sprite_mode = single_sprite_mode;
	self.animate_just_one_time = animate_just_one_time;
	
	nine_slice_part_00 = sprite_array[0];
	if(!single_sprite_mode){
	    nine_slice_part_01 = sprite_array[1];
	    nine_slice_part_02 = sprite_array[2];
	    nine_slice_part_10 = sprite_array[3];
	    nine_slice_part_11 = sprite_array[4];
	    nine_slice_part_12 = sprite_array[5];
	    nine_slice_part_20 = sprite_array[6];
	    nine_slice_part_21 = sprite_array[7];
	    nine_slice_part_22 = sprite_array[8];
	}

    if(!single_sprite_mode){
        vertical_strech_scale_factor = (height - (sprite_get_height(nine_slice_part_00) + sprite_get_height(nine_slice_part_20)))/sprite_get_height(nine_slice_part_10);

        horizontal_strech_scale_factor = (width - (sprite_get_width(nine_slice_part_00) + sprite_get_width(nine_slice_part_02)))/sprite_get_width(nine_slice_part_01);

        slice_2_x0 = sprite_get_width(nine_slice_part_00);
        slice_3_x0 = slice_2_x0 + sprite_get_width(nine_slice_part_01)*horizontal_strech_scale_factor;
        slice_4_y0 = sprite_get_height(nine_slice_part_00);
        slice_7_y0 = slice_4_y0 + sprite_get_height(nine_slice_part_10)*vertical_strech_scale_factor;    
    }
    else{                
        single_part_width = sprite_get_width(nine_slice_part_00) / 3;
        single_part_height = sprite_get_height(nine_slice_part_00) / 3;

        vertical_strech_scale_factor = (height - (2 * single_part_height))/single_part_height;

        horizontal_strech_scale_factor = (width - (2 * single_part_width))/single_part_width;        
        slice_2_x0 = single_part_width;
        slice_3_x0 = slice_2_x0 + single_part_width*horizontal_strech_scale_factor;
        slice_4_y0 = single_part_height;
        slice_7_y0 = slice_4_y0 + single_part_height*vertical_strech_scale_factor;            
    }
	
	sprite_index = nine_slice_part_00;
	image_xscale = width/sprite_width;
	image_yscale = height/sprite_height;
	can_draw = true;
}

return ret_obj;
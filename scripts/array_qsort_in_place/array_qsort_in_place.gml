/// @function array_qsort_in_place(array)
/// @description Sort the array using Quick Sort
/// @param {array} array The array to be qsorted

var array = argument[0];
var size = array_length_1d(array);
var pivot = 0;
var splitP = 0;

var pivotStack = [];
//var leftStack = []; //No need, always Pivot+1
var rightStack = [];
var pivotStackC = -1;

if(size > 1){ //Else -> Already Sorted or Empty
	var left, right;
	
	//Stacks the first pivot without working on it's left side
	pivotStack[++pivotStackC] = pivot;
	//leftStack[pivotStackC] = pivot+1; //No need, always Pivot+1
	rightStack[pivotStackC] = size-1;
	
	//Starts QSort - Keep it while we have pivots on the stack
	while(pivotStackC >= 0){
		pivot = pivotStack[pivotStackC]; //Removes the last pivot from the stack (time to work on the right side of it)
		
		left = pivot + 1; //left = leftStack[pivotStackC];
		right = rightStack[pivotStackC];
			
		//Pivot Moved
		splitP = array_qsort_search_and_fix(array, pivot, left, right);
		if(pivot != splitP){
			array_swap_in_place(array, pivot, splitP);
		}
			
		//Consumes this Pivot
		pivotStackC--;
		
		//Stack Right
		if(splitP < right){ //If it has right side
			pivotStack[++pivotStackC] = splitP+1;
			//leftStack[pivotStackC] = splitP+2; //No need, always Pivot+1
			rightStack[pivotStackC] = right;
		}
		
		//Stack Left
		if(splitP > left){ //If it has left side
			pivotStack[++pivotStackC] = pivot;
			//leftStack[pivotStackC] = left; //No need, always Pivot+1
			rightStack[pivotStackC] = splitP-1;
		}
	}
}
/// @function array_addSorted_in_place(array, value, unique)
/// @description Adds an number to the array in a sorted way (assumes it has only numbers and is already sorted)
/// @param {array} array The array in which the value will be added to
/// @param {real} value The value to be added to the array
/// @param {boolean} [unique=true] Adds thinking about uniqueness, also already assumes it has only unique numbers in the set

//Don't forget, this can only be used with arrays that hold numerical values!

var array = argument[0];
var value = argument[1];
var unique = argument_count > 2 ? argument[2] : true;

var size = array_length_1d(array);
var added = false;
var i;

for(i=0; i<size; i++){ //We run only until size, since the last doesn't need to be worked over
	if(unique && value == array[i]){
		added = true;
		break;
	} else if(!added && value < array[i]){
		array[@ size] = value; //Put the new value in the end, temporarilly. Array stays with size+1 elements
		added = true;
	}

	if(added){
		array_swap_in_place(array, i, size);
	}
}

if(!added){ //Cases where it should stay on the end
	array[@ size] = value;
}
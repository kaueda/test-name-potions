/// @description Add a pivot object.
/// @param animation
/// @param pivot

var _animation, _pivot;

_animation = argument[0];
_pivot = argument[1];


if(_animation.object_index == obj_anima_rotate){
	_animation.has_point_of_rotation = true;
	_animation.pivot_object = _pivot;
	_animation.pivot_coords_are_absolute = false;
}
else if(_animation.object_index == obj_anima_scale){
	anima_add_pivot_point(_animation, _pivot.x, _pivot.y, true);
}
else if(_animation.object_index == obj_anima_slide){
	//anima_add_pivot_point(_animation, _pivot.x, _pivot.y, true);
	show_debug_message("Slide not supported yet");
}
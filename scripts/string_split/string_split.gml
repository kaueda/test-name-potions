/// @function string_split(string_to_split, separator)
/// @description Splita o texto usando um separador e retorna o array com as partes
/// @param {string} string_to_split A string a ser quebrada
/// @param {string} separator O separador que será usado para a quebra

var string_to_split, separator;
var char_read, end_pos, str_length, 

var string_part_no = 0;
var start_pos = 1;
var ret = 0;

string_to_split = argument[0];
separator = argument[1];
str_length = string_length(string_to_split);

for(var i = 1; i <= str_length; i++){
    char_read = string_char_at(string_to_split, i);
    if ( (char_read == separator) || (i == str_length) ){    
        //if ((i > 1)){
            if(char_read == separator) {
                end_pos = i - 1;
			} else {
                end_pos = i;
			}
            ret[string_part_no] = string_copy(string_to_split, start_pos, end_pos - start_pos + 1);
            string_part_no++;
        //}
        start_pos = i + 1;
    }
}

return ret;
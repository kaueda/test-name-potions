/// @function draw_text_inline_init()
/// @description Bounds local variables to the shader uniforms. This must be executed before any shader call (recommended to do this on the create event of the object).
uni_size = shader_get_uniform(shd_text_outline, "size");
uni_thick = shader_get_uniform(shd_text_outline, "thick");
uni_icolor = shader_get_uniform(shd_text_outline, "iColor");
uni_ocolor = shader_get_uniform(shd_text_outline, "oColor");
uni_acc = shader_get_uniform(shd_text_outline, "accuracy");
uni_tol = shader_get_uniform(shd_text_outline, "tol");
uni_uvs = shader_get_uniform(shd_text_outline, "uvs");
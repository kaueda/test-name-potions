/// @function sm_is_mute_Sound()
/// @description Checks if the game's sounds are muted

var ctrl = obj_soundManager;
var ret = false; // We need to use With for the SD game's cases

with(ctrl){
	ret = !soundEnabled;
}
return ret;
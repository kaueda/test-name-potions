/// @function get_opening_command(string)
/// @description Gets the opening command from the string
/// @param {char} string The string to get from

var command_string = argument[0];
return string_copy(command_string, 2, string_length(command_string) - 1);
/// @function add_roi_event(roi, roi_event, ext_event)
/// @description Adds an event to be executed, from an object, when the corresponding event happens on the Roi
/// @param {object} roi An already instanced object of roi (with create_roi)
/// @param {macro} roi_event Choose between (0:MOUSE_ENTER, 1:MOUSE_LEAVE, 2:MOUSE_DOWN, 3:MOUSE_UP)
/// @param {array} [ext_event] An array with the external event to be called [object, event_type, event_number]

var roi = argument[0];
var roi_event = argument[1];
var ext_event = argument_count-1 >= 2 ? argument[2] : ma(noone, noone, noone);

var current_event;

current_event = roi.mouse_events[roi_event];

current_event[? "has_event"] = true;
current_event[? "obj"] = ext_event[0];
current_event[? "type"] = ext_event[1];
current_event[? "number"] = ext_event[2];

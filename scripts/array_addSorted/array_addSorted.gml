/// @function array_addSorted(array, value, unique)
/// @description Adds an number to the array in a sorted way, returning the new array (assumes it has only numbers and is already sorted)
/// @param {array} array The array in which the value will be added to
/// @param {real} value The value to be added to the array
/// @param {boolean} [unique=true] Adds thinking about uniqueness, also already assumes it has only unique numbers in the set

//Don't forget, this can only be used with arrays that hold numerical values!

var newArray = array_copyAll(argument[0]);

if(argument_count > 2)
	array_addSorted_in_place(newArray, argument[1], argument[2]);
else
	array_addSorted_in_place(newArray, argument[1]);

return newArray;

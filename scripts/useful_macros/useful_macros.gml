/// @function useful_macros(particular_macro_creation)
/// @description Creates all common macros from Useful Scripts and calls "particular macro creation" script
/// @param {script} [particular_macro_creation] Any script with custom macros for an specific game

var particular_scr = argument_count-1 >= 0 ? argument[0] : noone;

#macro _u undefined

if(script_exists(particular_scr)){
	script_execute(particular_scr);
}
/// @function draw_text_inline(x, y, text, font, thickness, outColor, outAlpha, accuracy, tolerance)
/// @description Creates an outline over the text. This can only be used during a draw event!
/// @param {integer} x The x position to draw the text at
/// @param {integer} y The y position to draw the text at
/// @param {string} text The text to be draw
/// @param {font} font The font that will be used in the text draw
/// @param {integer} [thickness=1] The thickness of the outline in pixels
/// @param {color} [outColor=c_black] The color of the outline
/// @param {integer} [outAlpha=1] The alpha of the outline (independent of the already set alpha)
/// @param {integer} [accuracy=8] The precision of the pixel check. This number can be either 8 or 4
/// @param {integer} [tolerance=0] The tolerance of alpha to be considered completely transparent, use only if needed

var xx = argument[0]; //X
var yy = argument[1]; //Y
var text = argument[2]; //Text
var font = argument[3]; //Font
var sep = -1; //Sep
var width = -1; //Width
var thickness = argument_count > 4 ? argument[4] : 1; //Thickness
var oColor = argument_count > 5 ? argument[5] : c_black; //outColor
var outalpha = argument_count > 6 ? argument[6] : 1; //outAlpha
var acc = argument_count > 7 ? argument[9] : 7; //Accuracy
var tol = argument_count > 8 ? argument[8] : 0; //Tolerance

draw_text_inline_ext(xx, yy, text, font, -1, -1, thickness, oColor, outalpha, acc, tol);
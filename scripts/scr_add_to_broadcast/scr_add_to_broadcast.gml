/// @function scr_add_to_broadcast(clock_obj, object_to_add, ev_type, ev_number)
/// @description Adds the object to the clock broadcast list
/// @param {object} clock_obj The manager clock object
/// @param {object} object_to_add The object that you want to add to the broadcast
/// @param {constant} ev_type The event type that will be called
/// @param {constant} ev_number The event number that will be called

var clockList = argument[0].broadcast_list;
ds_list_add(clockList, ma(argument[1], argument[2], argument[3]));
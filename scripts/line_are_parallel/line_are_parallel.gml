/// @function line_are_parallel(ax, ay, bx, by, cx, cy, dx, dy);
/// @description Checks whether or not two lines are parallel
/// @param {real} ax The X of the first point (a) of the first line
/// @param {real} ay The Y of the first point (a) of the first line
/// @param {real} bx The X of the first point (b) of the first line
/// @param {real} by The Y of the first point (b) of the first line
/// @param {real} cx The X of the first point (c) of the first line
/// @param {real} cy The Y of the first point (c) of the first line
/// @param {real} dx The X of the first point (d) of the first line
/// @param {real} dy The Y of the first point (d) of the first line

//Line 2
var ax = argument[0];
var ay = argument[1];
var bx = argument[2];
var by = argument[3];

//Line 1
var cx = argument[4];
var cy = argument[5];
var dx = argument[6];
var dy = argument[7];

var m1 = (by-ay)/(bx-ax); //Slope 1
var m2 = (dy-cy)/(dx-cx); //Slope 2

return m1 == m2;
// desc return sorted keys from a dictionary
var _map = argument[0];

var keys_arr, vals_arr;
var map_n = ds_map_size(_map);
keys_arr[map_n-1] = 0;
vals_arr[map_n-1] = 0;

var ikey = ds_map_find_first(_map);
for (var i = 0; i < map_n; i++) {
	keys_arr[i] = ikey;
	vals_arr[i] = _map[? ikey];

	ikey = ds_map_find_next(_map, ikey);
}

var res = scr_merge_sort(vals_arr, map_n, true, keys_arr);
return res[1];

/// @function save_ini_progress(iniFileArray, key_value_default_Array, inverted)
/// @description Saves the data in the array "key_value_default_Array" (in the format [key, value, default]), checking if it is considered progress.
/// The default progress is considered bigger values, but it can be inverted.
/// @param {array} iniFileArray The array created when save_init was called or an array of the form [iniPath, userSection]
/// @param {array} key_value_default_Array An array of key/value/default pair (also arrays) of the fields to be saved at the passed section
/// @param {boolean} [inverted=false] Should the progress comparation be inverted? The default is BIGGER

// Example: save_ini_progress(global.iniData, [["points", 100, 0], ["levels", 1, 0], ["badges", 5, 0]]);

var iniFileArray = argument[0];
var kvdArray = argument[1];
var inverted = argument_count > 2 ? argument[2] : false;
var tmpArray, oldValue;

ini_open(iniFileArray[0]);

for(var i=0; i<array_length_1d(kvdArray); i++){
	tmpArray = kvdArray[i];
	oldValue = ini_read_real(iniFileArray[1], tmpArray[0], tmpArray[2]);
	
	if((!inverted && tmpArray[1] > oldValue) || (inverted && tmpArray[1] < oldValue)){
		ini_write_real(iniFileArray[1], tmpArray[0], tmpArray[1]);
	}
}

ini_close();
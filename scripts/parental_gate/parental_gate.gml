/// @function parental_gate(parentalGate_link, parentalGate_layer, fadeDuration, backgroundDismiss, common_link)
/// @description Creates a Parental Gate for a link, if on mobile (PC VERSION MOCKS ANDROID VERSION!)
/// @param parentalGate_link The link that will be used on the Mobile version, that will require a Parental Gate
/// @param parentalGate_layer The layer in which the parental gate will be created
/// @param [fadeDuration=0.3] The link that will be used on the Mobile version, that will require a Parental Gate
/// @param [backgroundDismiss=false] The link that will be used on the Mobile version, that will require a Parental Gate
/// @param [web_link="thirdpartylicenses.html"] The link that will be used in the Web version, that doesn't require a Parental Gate

var pLink = argument[0];
var pLayer = argument[1];
var f_dur = argument_count > 2 ? argument[2] : 0.3;
var bg_dis = argument_count > 3 ? argument[3] : false;
var cLink = argument_count > 4 ? argument[4] : "thirdpartylicenses.html";

if(os_browser != browser_not_a_browser || (os_type != os_android && os_type != os_ios && os_type != os_windows)){
    url_open_ext(cLink, "_blank");
	
	return noone;
} else {
    var parental = instance_create_depth(0, 0, layer_get_depth(pLayer)-1, obj_parental_gate_panel);
    parental.site_to_go = pLink;
	parental.fadeDuration = f_dur;
	parental.backgroundDismiss = bg_dis;
	
	return parental;
}
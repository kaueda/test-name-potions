/// @function scr_time_broadcast(clock_obj)
/// @description Broadcasts the clock to all objects
/// @param {object} clock_obj The manager clock object

var clockList = argument[0].broadcast_list;
var size = ds_list_size(clockList);
var tmp_arr = 0;

for(var i=0; i<size; i++){
	tmp_arr = clockList[| i];
		
	with(tmp_arr[0]){
		event_perform(tmp_arr[1], tmp_arr[2]);
	}
}
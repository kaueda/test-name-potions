/// @description Rotate an object acording to his own origin point or acording to a specified pivot (which can be a point in space or an object).
/// @param target_object
/// @param final_angle
/// @param duration_in_secs
/// @param loop
/// @param start_animation_on_stop
/// @param [easing_script] easing_script

var target_obj, final_angle, duration, loop, destroy_after_use, easing_script, start_on_stop;
var ret;

target_obj = argument[0];
final_angle = argument[1];
duration = argument[2] * 1000000;
loop = argument[3];
destroy_after_use = !loop;
start_on_stop = argument[4];
easing_script = anima_ease_linear;   


if(argument_count == 6){			
	easing_script = argument[6];
}

ret = instance_create_depth(0, 0, 0, obj_anima_rotate);

ret.target_obj = target_obj;
ret.no_of_values = 2;
ret.final_value[0] = degtorad(final_angle);
ret.easing = easing_script;
ret.duration = duration;
ret.loop = loop;
ret.destroy_after_use = destroy_after_use;
ret.is_by = false;
ret.has_point_of_rotation = false;
ret.rotation_point_x = 0;
ret.rotation_point_y = 0;
ret.pivot_coords_are_absolute = false;
ret.pivot_object = noone;

if(!start_on_stop){
    ret.can_animate = true;
}

return ret;

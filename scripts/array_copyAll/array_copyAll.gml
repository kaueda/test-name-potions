/// @function array_copyAll(array)
/// @description Copies all elements of an array to a new array and returns it
/// @param {array} array The array to be copied

var srcArray = argument[0];
var array = [];
array_copy(array, 0, srcArray, 0, array_length_1d(srcArray));

return array;
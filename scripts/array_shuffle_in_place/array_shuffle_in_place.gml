/// @function array_shuffle_in_place(array)
/// @description Shuffles the array
/// @param {array} array The array to be shuffled

//Fisher–Yates shuffle -> Durstenfeld's version
var array = argument[0];
var size = array_length_1d(array);

for(var i=size-1; i>0; i--){
	array_swap_in_place(array, i, irandom_range(0, i));
}
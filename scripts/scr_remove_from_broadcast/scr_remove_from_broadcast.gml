/// @function scr_remove_from_broadcast(clock_obj, object_to_add)
/// @description Removes the object from the clock broadcast list
/// @param {object} clock_obj The manager clock object
/// @param {object} object_to_add The object that want to remove from the broadcast

var clockList = argument[0].broadcast_list;
var pos = ds_list_find_index(clockList, argument[1]);
ds_list_delete(clockList, pos);
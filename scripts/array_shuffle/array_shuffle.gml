/// @function array_shuffle_in_place(array, iterations)
/// @description Shuffles the array, returning the new array
/// @param {array} array The array to be shuffled
/// @param {integer} [iterations=1] The number of times the array will be rolled

//Fisher–Yates shuffle -> Durstenfeld's version
var newArray = array_copyAll(argument[0]);
array_shuffle_in_place(newArray);

return newArray;

// desc merge function for merge sort

var arr = argument[0];
var left = argument[1];
var right = argument[2];
var mid = argument[3];

var is_map_sort = false;
if (argument_count > 4) {
	is_map_sort = argument[4];
	var karr = argument[5];
}

var i, j, k;
var n1 = mid - left + 1;
var n2 =  right - mid;

var arr_left, arr_right;
if (is_map_sort) var karr_left, karr_right;

for (i = n1-1; i >= 0; i--) {
	arr_left[i] = arr[i + left];
	if (is_map_sort) karr_left[i] = karr[i + left];
}
for (j = n2-1; j >= 0; j--) {
	arr_right[j] = arr[j + mid + 1];
	if (is_map_sort) karr_right[j] = karr[j + mid + 1];
}

i = 0; j = 0; k = left;
while (i < n1 && j < n2) {
    if (arr_left[i] >= arr_right[j]) {
		arr[k] = arr_left[i];
		if (is_map_sort) karr[k] = karr_left[i];
		
		i++;
	} else {
		arr[k] = arr_right[j];
		if (is_map_sort) karr[k] = karr_right[j];
		
		j++;
	}
	
	k++;
}

while (i < n1) {
	arr[k] = arr_left[i];
	if (is_map_sort) karr[k] = karr_left[i];
	
	k++; i++;
}
while (j < n2) {
	arr[k] = arr_right[j];
	if (is_map_sort) karr[k] = karr_right[j];
	
	k++; j++;
}

if (!is_map_sort)
	return arr;
else
	return [arr, karr];
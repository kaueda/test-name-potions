/// @function scr_removeFeatures(currentFeature, removingFeatures)
/// @description Removes one or more features from the current feature
/// @param currentFeature The current feature to remove from
/// @param removingFeatures The Feature (or coumpound) to be removed

/// Usage: var resultingState = scr_removeFeatures(currentFeature, (1 << ON_AIR) & (1 << FALLING));
//return ~(argument[0] ^ argument[1]);
return argument[0] & ~argument[1];
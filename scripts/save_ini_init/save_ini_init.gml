/// @function save_ini_init(iniPath)
/// @description Returns an array of strings with the following [iniPath, userSection] by 
/// using the "get_user_metadata" and setting the section based on the users Username and ID
/// @param {string} iniPath The path to save/open the ini file

// Example: save_ini_init("save.ini");

var iniPath = argument[0];

var debug = true;
var ret = [iniPath, "Not_LoggedIn_Save"];  /*Not Logged In Player*/

if(os_type != os_android && os_type != os_ios){
    var userData = get_user_metadata();
	var valid = is_string(userData) && userData != "NOUSER";
	
    if(valid){
        userData = json_decode(userData);
    } else if(debug && os_type != os_browser && os_type == os_windows){ //Used for Debugging
        userData = json_decode(@'{ "playerJSON" :{"id":"0001", "name":"TestUser", "email":"testuser@gmail.com", "bday":"01/01/1879" }}');
    }
	
	if(valid){
	    userData = userData[? "playerJSON"];
	    ret[1] = userData[? "name"] + " - " + userData[? "id"];

		ds_map_destroy(userData);
	}
}
//elses take the section to "Not_LoggedIn_Save"

return ret;
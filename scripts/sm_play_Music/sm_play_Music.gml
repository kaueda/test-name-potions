/// @function sm_play_Music(musicIndex, loops, fadeDuration[0.3], intro[noone])
/// @description Plays a Music, fading in itself and fading out any other playing music.
/// @param {sound} musicIndex The music you want to play.
/// @param {boolean} [loops=true] If the music should loop or not.
/// @param {float} fadeDuration The fade in duration of the music (and fade out of the current one, if any is playing).
/// @param {sound} [intro=noone] The intro sound asset, if the music has an intro.

var music = argument[0];
var loops = argument_count > 1 ? argument[1] : true;
var fadeT = argument_count > 2 ? argument[2] : 0.3;
var intro = argument_count > 3 ? argument[3] : noone;

var ctrl = obj_soundManager;
with(ctrl){
	if(!audio_is_playing(currentMusic)){  //Start a new song
	    playingIntro = false;
	    introLength = -1;
	    if(intro != noone){ //intro
	        var nMusic = audio_play_sound(intro, 0, false);
	        audio_sound_gain(nMusic, musicVolume, 0);
			
	        currentMusic = nMusic;
	        playingIntro = true;
	        introLength = audio_sound_length(intro) - (1/room_speed);
	        realMusic = music;
	        realMusicFade = fadeT;
	        realMusicLoop = loops;
			realMusicIntro = nMusic;
	    } else { //actual music
	        var nMusic = audio_play_sound(music, 0, loops);

			if(realMusicIntro == noone){ //Not Coming from Intro
		        audio_sound_gain(nMusic, 0, 0);
		        audio_sound_gain(nMusic, musicVolume, fadeT*1000);
			} else { //Coming from intro
				audio_sound_gain(nMusic, introGain, 0);
				if(onStop != noone){ //But stopping!
					audio_sound_gain(nMusic, 0, (alarm[0]/room_speed)*1000);
					onStop = nMusic;
				}
				introGain = 0;
			}

	        currentMusic = nMusic;
			
			realMusic = noone;
			realMusicFade = noone;
			realMusicLoop = noone;
			realMusicIntro = noone;
	    }
	} else if(!audio_is_playing(music) && !audio_is_playing(intro)){ //Stop a song before start a new one
	    newMusic = music;
	    newMusicFade = fadeT;
	    newMusicLoop = loops;
	    newMusicIntro = intro;
        
	    sm_stop_Music(currentMusic, fadeT);
	}
}
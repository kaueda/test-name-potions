/// @function create_label(text, x, y, font, color, time_alive, alignment)
/// @description Cria uma label local, com uma cor por um tempo
/// @param {string} text O texto da label
/// @param {real} x A posição x da label
/// @param {real} y A posição y da label
/// @param {resource} font A fonte a ser utilizada na label
/// @param {color} color A cor a ser utilizada na label
/// @param {integer} [time_alive=infinite] Duração em segundos da label
/// @param {array} [alignment] Array contendo alinhamento [halign, valign]

var ret = noone;
var _time = argument_count-1 >= 5 ? argument[5] : -1;
var _alignment = argument_count-1 >= 6 ? argument[6] : ma(noone, noone);

var _label_text = argument[0];
var _label_x = argument[1];
var _label_y = argument[2];
var _label_font = argument[3];
var _label_color = argument[4];

/*if(argument_count == 7){
    _time = ;   
    _alignment = ;          
}
else if(argument_count == 6){
    _time = argument[5];   
}*/

ret = instance_create_depth(_label_x, _label_y, depth, obj_label);
with(ret){
    label_text = _label_text;
    label_x = _label_x;
    label_y = _label_y;
    label_font = _label_font;
    time = _time;   
    label_color = _label_color;
    halign = _alignment[0];
    valign = _alignment[1];
    event_perform(ev_other, ev_user0);
}

return ret;

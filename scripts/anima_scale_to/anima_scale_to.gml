/// @function anima_scale_to(target_object, final_x_scale, final_y_scale, duration_in_secs, loop, start_animation_on_stop [, easing_script])
/// @description Scales objects to given scales.
/// @param target_object The object to be scaled.
/// @param final_x_scale The object's final x scale.
/// @param final_y_scale The object's final y scale.
/// @param duration_in_secs The duration of the scale.
/// @param loop If set to true, the animation will loop.
/// @param start_animation_on_stop If set to true the animation must be manually started.
/// @param [easing_script] easing_script The easing script used to perform the scale.

var target_obj, final_x, final_y, duration, loop, destroy_after_use, easing_script, start_on_stop;
var ret;

target_obj = argument[0];
final_x = argument[1];
final_y = argument[2];
duration = argument[3] * 1000000;
loop = argument[4];
destroy_after_use = !loop;
start_on_stop = argument[5];
if(argument_count == 7){
    easing_script = argument[6];   
}
else{
    easing_script = anima_ease_linear;
}
ret = instance_create_depth(0, 0, 0, obj_anima_scale);

ret.target_obj = target_obj;
ret.no_of_values = 2;
ret.final_value[0] = final_x;
ret.final_value[1] = final_y;
ret.easing = easing_script;
ret.duration = duration;
ret.loop = loop;
ret.destroy_after_use = destroy_after_use;
ret.is_by = false;
ret.pivot_point = false;

if(!start_on_stop){
    ret.can_animate = true;
}

return ret;

/// @description Scales objects by given scales.
/// @param target_object The object to be scaled.
/// @param x_scale_variation	The object's x scale variation.
/// @param y_scale_variation	The object's y scale variation.
/// @param duration_in_secs The duration of the scale.
/// @param loop If set to true, the animation will loop.
/// @param start_animation_on_stop If set to true the animation must be manually started.
/// @param [easing_script] easing_script The easing script used to perform the scale.

var target_obj, x_displacement, y_displacement, duration, loop, destroy_after_use, easing_script, start_on_stop;
var ret;

target_obj = argument[0];
x_displacement = argument[1];
y_displacement = argument[2];
duration = argument[3] * 1000000;
loop = argument[4];
destroy_after_use = !loop;
start_on_stop = argument[5];
if(argument_count == 7){
    easing_script = argument[6];   
}
else{
    easing_script = anima_ease_linear;
}

ret = instance_create_depth(0, 0, 0, obj_anima_scale);

ret.target_obj = target_obj;
ret.no_of_values = 2;
ret.value_difference[0] = x_displacement;
ret.value_difference[1] = y_displacement;
ret.easing = easing_script;
ret.duration = duration;
ret.loop = loop;
ret.destroy_after_use = destroy_after_use;
ret.is_by = true;

if(!start_on_stop){
    ret.can_animate = true;
}

return ret;

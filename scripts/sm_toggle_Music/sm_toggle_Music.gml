/// @function sm_toggle_Music(fadeTime)
/// @description Mutes/Unmutes the game's music using a fade out
/// @param {float} [fadeTime=0.3] The duration of the fade out

var fadeT = argument_count > 0 ? argument[0] : 0.3;

var ctrl = obj_soundManager;
with(ctrl){
    if(musicVolume == 1){
        musicVolume = 0;
    } else {
        musicVolume = 1;
    }
    audio_sound_gain(currentMusic, musicVolume, fadeT*1000);
}
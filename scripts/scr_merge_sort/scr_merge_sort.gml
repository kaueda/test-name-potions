// @desc simple merge sort
var arr = argument[0];
var arr_n = argument[1];

var is_map_sort = false;
if (argument_count > 2) {
	is_map_sort = argument[2];
	var karr = argument[3];
}

var curr_size;  // For current size of subarrays to be merged
                   // curr_size varies from 1 to n/2
var left_start; // For picking starting index of left subarray
                // to be merged
 
// Merge subarrays in bottom up manner.  First merge subarrays of
// size 1 to create sorted subarrays of size 2, then merge subarrays
// of size 2 to create sorted subarrays of size 4, and so on.
for (curr_size = 1; curr_size <= arr_n - 1; curr_size = 2 * curr_size) {
    // Pick starting point of different subarrays of current size
    for (left_start = 0; left_start < arr_n - 1; left_start += 2 * curr_size) {
        // Find ending point of left subarray. mid+1 is starting 
        // point of right
        var mid = left_start + curr_size - 1;
        var right_end = min(left_start - 1 + (2*curr_size), arr_n - 1);
 
        // Merge Subarrays arr[left_start...mid] & arr[mid+1...right_end]
		if (!is_map_sort) {
			arr = scr_merging(arr, left_start, right_end, mid);
		} else {
			var double_arr = scr_merging(arr, left_start, right_end, mid, is_map_sort, karr);
			arr = double_arr[0];
			karr = double_arr[1];
		}
    }
}

if (!is_map_sort) 
	return arr;
else
	return double_arr;
/// @description Anchor to Camera/Screen
var scale;
if(anchor_enable){
	if(follow_cam_ratio){
		scale = camera_get_view_width(camera)/camera_get_view_height(camera);
	} else {
		scale = 1;
	}	
	
    target.x = camera_get_view_x(camera) + (initial_x * scale);
    target.y = camera_get_view_y(camera) + (initial_y * scale);
	
	target.image_xscale = scale;
	target.image_yscale = scale;
}
/// @description plant setup

type = irandom(global.n_atts - 1);
att_value = (irandom(2) + 1) * 5;

var nper_tier = global.n_plants / global.n_atts;
part = global.plants_parts[irandom(global.n_plants_parts - 1)];
name = global.plants[irandom(6) + (type * global.n_plants / 4)];

dragging = false
mx_offset = 0;
my_offset = 0;
{
    "id": "2e18b41b-473d-47d8-8c9c-708ba67749fc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_plant",
    "eventList": [
        {
            "id": "3f9e541d-35ae-43f5-91d0-a6d004e1dd79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2e18b41b-473d-47d8-8c9c-708ba67749fc"
        },
        {
            "id": "aad06518-03b3-4e58-bae0-1e57638bd577",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2e18b41b-473d-47d8-8c9c-708ba67749fc"
        },
        {
            "id": "63717196-8185-47fd-b3e5-98f2e5c60af4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2e18b41b-473d-47d8-8c9c-708ba67749fc"
        },
        {
            "id": "555860f8-5d06-4130-9bdc-b33124007c3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "2e18b41b-473d-47d8-8c9c-708ba67749fc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e4fcdeb-2212-46f0-9ca9-7d59abb387dc",
    "visible": true
}
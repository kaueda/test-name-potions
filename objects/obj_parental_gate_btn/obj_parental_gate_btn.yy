{
    "id": "c6bbbcee-b10f-4086-ac03-f8b9c135138f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_parental_gate_btn",
    "eventList": [
        {
            "id": "3bc6aa47-a667-4191-b823-c8bf5d41fbbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c6bbbcee-b10f-4086-ac03-f8b9c135138f"
        },
        {
            "id": "f1d1fc9e-220d-4dfd-8980-eefd11bfe137",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c6bbbcee-b10f-4086-ac03-f8b9c135138f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0dc7e61-a217-483b-abf1-f913973a3006",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "868515c7-ba07-443b-8078-f7d47aec9d1b",
    "visible": true
}
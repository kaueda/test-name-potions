/// @description Check Multiple Clicks

if(enabled){
    //if(position_meeting(mouse_x, mouse_y, id)){
    mouse_on_top = false;
    if(position_meeting(device_mouse_x(0), device_mouse_y(0), self)){
        activating_device = 0;
        mouse_on_top = true;
    }
    else if(position_meeting(device_mouse_x(1), device_mouse_y(1), self)){
        activating_device = 1;    
        mouse_on_top = true;
    }
    else if(position_meeting(device_mouse_x(2), device_mouse_y(2), self)){
        activating_device = 2;    
        mouse_on_top = true;
    }
    if(!mouse_on_top || !device_mouse_check_button(activating_device, mb_left)){
        image_index = 0;
    }
    else{
        image_index = 1;
    }    
}
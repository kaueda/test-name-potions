draw_set_color(c_white);
draw_set_font(fnt_items);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
var display_name = name + " " + part + " +" + string(att_value) + " " + global.atts_names[type];				   
var txt_width = string_width(display_name) + 10;
var txt_height = string_height(display_name) + 10;

image_xscale = txt_width/64;
image_yscale = txt_height/64;

draw_self();
draw_text(x + txt_width/2, y + txt_height/2, display_name);

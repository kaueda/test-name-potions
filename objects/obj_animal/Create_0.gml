/// @description plant setup

type = irandom(global.n_atts - 1);
att_value = (irandom(2) + 1) * 5;

var nper_tier = global.n_animals / global.n_atts;
part = global.animals_parts[irandom(global.n_animals_parts - 1)];
name = global.animals[irandom(nper_tier - 1) + (type * nper_tier)];

dragging = false
mx_offset = 0;
my_offset = 0;
{
    "id": "827cde15-5c57-4694-83d3-bf6b5e7368a7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_animal",
    "eventList": [
        {
            "id": "446ae68c-fcc9-4bee-8a90-7067e9d50139",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "827cde15-5c57-4694-83d3-bf6b5e7368a7"
        },
        {
            "id": "08d712cc-2bac-471e-bb21-8338cfa74082",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "827cde15-5c57-4694-83d3-bf6b5e7368a7"
        },
        {
            "id": "50395abb-79da-473c-976a-bdb400c6371f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "827cde15-5c57-4694-83d3-bf6b5e7368a7"
        },
        {
            "id": "6a85ba84-a0cf-4eaf-befb-5980e901cbf9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "827cde15-5c57-4694-83d3-bf6b5e7368a7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e4fcdeb-2212-46f0-9ca9-7d59abb387dc",
    "visible": true
}
/// @description Setup Potion Mechanics

#region Attributes
global.fibbs_rates = [5, 10, 15, 25, 40, 65, 105, 170];
global.atts_names = ["Fire", "Water", "Earth", "Air"];
global.n_atts = array_length_1d(global.atts_names);

global.atts_mix_names = ["Steam", "Magma", "Thunder", "Mud", "Storm", "Sandstorm"];
global.n_atts_mix = array_length_1d(global.atts_mix_names);

#region Elemental Attributes
global.atts_n_tiers = 8;

global.fire_nouns = ["Kindle", "Flame", "Flare", "Fire", "Fireball", "Wildfire", "Blaze", "Inferno"];
global.fire_adjs = ["Ardent", "Sizzling", "Blistering", "Burning", "Blazing", "Searing", "Scorching", "Fiery"];

global.water_nouns = ["Sprinkle", "Drizzle", "Shower", "Water", "Rain", "Downpour", "Torrent", "Flood"];
global.water_adjs = ["Humid", "Dewed", "Drizzling", "Soggy", "Wet", "Doused", "Drenched", "Soaking"];

global.earth_nouns = ["Dust", "Dirt", "Sand", "Earth", "Rubble", "Rock", "Marble", "Bedrock"];
global.earth_adjs = ["Sturdy", "Solid", "Dense", "Concrete", "Compact", "Condensed", "Hulking", "Impenetrable"];

global.air_nouns = ["Breeze", "Draft", "Gust", "Air", "Wind", "Gale", "Typhoon", "Hurricane"];
global.air_adjs = ["Flimsy", "Light", "Weightless", "Aerial", "Windy", "Gusty", "Tempestuous", "Ethereal"];
#endregion
#endregion

#region Plants
global.plants = ["Allspice", "Angelica", "Basil", "Bay", "Burdock", "Calendula", "Caraway", "Chamomile", "Cinnamon", "Clove", "Comfrey", "Dill", "Ginger", "Ivy", "Jasmine", "Juniper", "Lavender", "Licorice", "Marjoram", "Mint", "Mugwort", "Nutmeg", "Parsley", "Peonies", "Rosemary", "Sage", "Thyme", "Verbena"];
global.n_plants = array_length_1d(global.plants);

global.plants_parts = ["Stem", "Leaf", "Flower", "Roots", "Bark", "Sap", "Berry", "Fruit", "Seed"];
global.n_plants_parts = array_length_1d(global.plants_parts);
#endregion

#region Animals
global.animals = ["Worms", "Snails", "Buterflies", "Ants", "Spiders", "Scorpions", "Crabs", "Shrimps", "Octopus", "Squid", "Shark", "Barracuda", "Salmon", "Frogs", "Salamandra", "Axolotl", "Bats", "Rats", "Rams", "Bears", "Wolfs", "Bunnies", "Lizards", "Geckos", "Snakes", "Eagles", "Crows", "Cucos"];
global.n_animals = array_length_1d(global.animals);

global.animals_parts = ["Fangs", "Claws", "Talons", "Wings", "Legs", "Arms", "Bones", "Hearts", "Blood", "Saliva", "Tears", "Venom", "Eggs", "Beaks"];
global.n_animals_parts = array_length_1d(global.animals_parts);
#endregion


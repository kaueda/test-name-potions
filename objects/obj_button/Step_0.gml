/// @description Event Dispatch
if(enabled){
    if(position_meeting(mouse_x, mouse_y, id)){
        image_index = 1;
		
        if(mouse_check_button(mb_left)){
            image_index = 2;
        } else if(mouse_check_button_released(mb_left)){
            if(isToggable){
                if(toggle){
                    sprite_index = primary_sprite;
                } else {
                    sprite_index = secondary_sprite;                
                }
                toggle = !toggle;
            }
			
            image_index = 1;
            event_perform(ev_other, ev_user0);            
        }
    } else{
        image_index = 0;
    }    
}
{
    "id": "da50c847-9a4a-44a4-9b06-fe4cbc6b857e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_anima_fill_camera",
    "eventList": [
        {
            "id": "f17c2aba-72f8-43c8-a312-764f2dd15cf8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "da50c847-9a4a-44a4-9b06-fe4cbc6b857e"
        },
        {
            "id": "e46112c2-e806-49af-acb5-7ad0266cc92a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "da50c847-9a4a-44a4-9b06-fe4cbc6b857e"
        },
        {
            "id": "d3a0a12f-dac1-436c-bb1e-43e30ae33fc5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "da50c847-9a4a-44a4-9b06-fe4cbc6b857e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
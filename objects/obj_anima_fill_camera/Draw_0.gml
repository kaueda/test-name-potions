if(!is_draw_gui && can_draw){
    prev_color = draw_get_colour();
    prev_alpha = draw_get_alpha();
    draw_set_colour(color);
    draw_set_alpha(image_alpha);
    draw_rectangle(camera_get_view_x(camera), camera_get_view_y(camera), 
		camera_get_view_x(camera) + camera_get_view_width(camera), 
			camera_get_view_y(camera) + camera_get_view_height(camera), false);
    draw_set_colour(prev_color);
    draw_set_alpha(prev_alpha);	
}


/// @description Draw
if(can_draw){
    if(valign != noone && halign != noone){
        draw_set_valign(valign);
        draw_set_halign(halign);       
    }
	
    prev_color = draw_get_colour();
    draw_set_font(label_font);
    draw_set_colour(label_color);
    draw_set_alpha(image_alpha);
	
    /*if(get_dims){
        ww = string_width(string_hash_to_newline(label_text));
        hh = string_height(string_hash_to_newline(label_text));
    }*/	
    draw_text(x, y, string_hash_to_newline(label_text));
	
    draw_set_colour(prev_color);
    draw_set_alpha(1);    
}


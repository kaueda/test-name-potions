{
    "id": "7eb8828d-a9ed-443b-8b48-c6659e18fd83",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_drag_controller",
    "eventList": [
        {
            "id": "06691d61-fe67-43e4-b2c1-1c692a08c064",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7eb8828d-a9ed-443b-8b48-c6659e18fd83"
        },
        {
            "id": "0e6040d2-777e-4e83-acc2-7fa1c42bf6cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7eb8828d-a9ed-443b-8b48-c6659e18fd83"
        },
        {
            "id": "15df3f54-f41b-4a19-b60b-851866520dc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "7eb8828d-a9ed-443b-8b48-c6659e18fd83"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}

if (instance_exists(global.instance_clicked)) {
	global.instance_clicked.image_blend = c_gray;   // set the colour so we can see the lock
	instance_locked = global.instance_clicked;      // lock onto this instance
	
	if (new_depth > -1*instance_number(instance_locked.object_index))
		new_depth = -1*instance_number(instance_locked.object_index);
	
	instance_locked.depth = new_depth--;            // pop the instance to the top
	inst_dx = instance_locked.x - mouse_x;          // get the difference between mouse and instance
	inst_dy = instance_locked.y - mouse_y;
	global.instance_clicked = noone;
}

if (instance_exists(instance_locked)) {
    instance_locked.x = mouse_x + inst_dx;        // get the new instance location by offsetting
    instance_locked.y = mouse_y + inst_dy;        // the mouse BACK to the instance   
}

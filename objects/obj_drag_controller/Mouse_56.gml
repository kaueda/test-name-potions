if (instance_exists(instance_locked)) {
	var inst_lock = instance_locked;
	var consumed = false;
	with(instance_locked) {
		if (place_meeting(x, y, obj_potionificator)) {
			with(obj_potionificator) {
				consumed = true;
				attributes[inst_lock.type] += inst_lock.att_value;
				
				last_added_att = inst_lock.type;
				last_added_att_val = inst_lock.att_value;
				event_perform(ev_other, ev_user0);
			}
		} else if (place_meeting(x, y, obj_deletor)) {
			consumed = true;
		}
	}

	if (consumed) instance_destroy(instance_locked);
	else instance_locked.image_blend = c_white;
	instance_locked = noone;
	
	inst_dx = 0;
	inst_dx = 0;
}

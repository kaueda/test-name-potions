{
    "id": "75773245-5c38-496d-92da-95b69d58d36c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_roi",
    "eventList": [
        {
            "id": "c130994a-7b2f-4288-9895-84956afbb818",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "75773245-5c38-496d-92da-95b69d58d36c"
        },
        {
            "id": "41795182-f746-45f4-a293-06fea225588f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "75773245-5c38-496d-92da-95b69d58d36c"
        },
        {
            "id": "b9f3d5b9-1fe3-401f-b81a-e5c201569945",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "75773245-5c38-496d-92da-95b69d58d36c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "867a8915-4b2d-4bea-bfdf-2ae084abd16c",
    "visible": false
}
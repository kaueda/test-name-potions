/// @description Perform added event
if(curr_event[? "has_event"]){
    if(instance_exists(curr_event[? "obj"])){
        with(curr_event[? "obj"]){
            event_perform(other.curr_event[? "type"], other.curr_event[? "number"]);
        }
        //dispached_event = true;
    }
}


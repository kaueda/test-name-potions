/// @description Checks and Changes States
if(enabled){
    if(position_meeting(mouse_x, mouse_y, id)){
        if(mouse_check_button(mb_left)){
            //Btn up
            if(last_event != 2){
                last_event = 2;
                //show_debug_message("Down em cima da região de interesse");
                curr_event = mouse_events[2];
                event_perform(ev_other, ev_user0);
            }              
        }
        else if(mouse_check_button_released(mb_left)){
            //Do btn click!
            if(last_event != 3){
                last_event = 3;
                //show_debug_message("Release em cima da região de interesse");
                curr_event = mouse_events[3];
                event_perform(ev_other, ev_user0);                
            }            
        }
        else{
            if(last_event != 0){
                last_event = 0;
                //show_debug_message("Cursor entrou em cima da região de interesse");
                curr_event = mouse_events[0];
                event_perform(ev_other, ev_user0);
            }
        }
    }
    else{
        // Pointer left actor
        if(last_event != 1){
            last_event = 1;
            //show_debug_message("Cursor saiu da região de interesse");
            curr_event = mouse_events[1];
            event_perform(ev_other, ev_user0);
        }
    }    
}
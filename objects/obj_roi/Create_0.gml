/// @description Events Map and Init Variables
curr_event = noone;
mouse_events = 0;
ammount_of_events = 4;

for(var i = 0; i < ammount_of_events; i++){
    mouse_events[i] = ds_map_create();
    curr_event = mouse_events[i];
    curr_event[? "has_event"] = false;
    curr_event[? "obj"] = noone;
    curr_event[? "type"] = noone;
    curr_event[? "number"] = noone;
}

enabled = true;
last_event = -1;


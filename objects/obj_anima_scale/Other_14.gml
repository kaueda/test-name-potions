if(!instance_exists(target_obj)){
    instance_destroy();
    exit;
}
target_obj.image_xscale = initial_value[0];
target_obj.image_yscale = initial_value[1];


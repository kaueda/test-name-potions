{
    "id": "864094db-2f90-4997-bb85-dc71ee70fdfa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_anima_scale",
    "eventList": [
        {
            "id": "ae7cd121-985c-4fa9-a379-1e8480b10622",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "864094db-2f90-4997-bb85-dc71ee70fdfa"
        },
        {
            "id": "0dda9c3a-001d-4222-b5b9-520a99b877b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "864094db-2f90-4997-bb85-dc71ee70fdfa"
        },
        {
            "id": "ab95214d-12ba-4d18-ac0b-b03c1b875eb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "864094db-2f90-4997-bb85-dc71ee70fdfa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b206dd7c-bdb2-430c-ab81-ed1aa612744f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}
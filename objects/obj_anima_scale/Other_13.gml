if(!instance_exists(target_obj)){
    instance_destroy();
    exit;
}
if(!is_by){
    initial_value[0] = target_obj.image_xscale;
    initial_value[1] = target_obj.image_yscale;
}
else{
    initial_value[0] = target_obj.image_xscale;
    initial_value[1] = target_obj.image_yscale;
    final_value[0] = initial_value[0] * value_difference[0];
    final_value[1] = initial_value[1] * value_difference[1];
    
}
value_difference[0] = final_value[0] - initial_value[0];
value_difference[1] = final_value[1] - initial_value[1];
event_inherited();


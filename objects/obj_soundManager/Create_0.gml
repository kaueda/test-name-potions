/// @description Create

currentMusic = noone;

realMusic = noone;
realMusicFade = noone;
realMusicLoop = noone;
realMusicIntro = noone;

newMusic = noone;
newMusicFade = noone;
newMusicLoop = noone;
newMusicIntro = noone;

playingIntro = false;
introLength = -1;
introGain = 0;

musicVolume = 1;
soundEnabled = true;
onStop = noone;
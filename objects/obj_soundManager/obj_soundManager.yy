{
    "id": "14c6892f-25ce-4dec-9ef2-50a1911a1932",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_soundManager",
    "eventList": [
        {
            "id": "70d9e3e3-44c8-4622-b8e6-6ceacc31f597",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "14c6892f-25ce-4dec-9ef2-50a1911a1932"
        },
        {
            "id": "773c8d14-6ad0-49e3-9f02-c6f25371fa6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "14c6892f-25ce-4dec-9ef2-50a1911a1932"
        },
        {
            "id": "61cca5b6-dcd4-40bb-9b4d-a2bab42cbb07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "14c6892f-25ce-4dec-9ef2-50a1911a1932"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
/// @description Wait for Intro to End

if(currentMusic != noone && playingIntro && (audio_sound_get_track_position(currentMusic) >= introLength || !audio_is_playing(currentMusic))){
	
	introGain = audio_sound_get_gain(currentMusic);
	audio_stop_sound(currentMusic);
	
    currentMusic = noone;
    playingIntro = false;
    introLength = -1;
	
    sm_play_Music(realMusic, realMusicLoop, 0); //Starts Loop
}
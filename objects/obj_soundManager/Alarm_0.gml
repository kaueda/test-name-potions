/// @description Substitute playing music

if(onStop != noone){
    audio_stop_sound(onStop);
    onStop = noone;
}

if(newMusic != noone){
    sm_play_Music(newMusic, newMusicLoop, newMusicFade, newMusicIntro);
	newMusic = noone;
	newMusicFade = noone;
	newMusicLoop = noone;
	newMusicIntro = noone;
}
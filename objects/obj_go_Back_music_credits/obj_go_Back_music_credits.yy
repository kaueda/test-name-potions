{
    "id": "674561a6-d87d-4953-9eb2-e49dd3cee835",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_go_Back_music_credits",
    "eventList": [
        {
            "id": "10f847e8-b0b4-4779-a907-961523ca7247",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "674561a6-d87d-4953-9eb2-e49dd3cee835"
        },
        {
            "id": "7699f209-c5a2-4a10-a6cf-5635f5a3a584",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "674561a6-d87d-4953-9eb2-e49dd3cee835"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0dc7e61-a217-483b-abf1-f913973a3006",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "833c4a94-73a8-4911-b961-c39de91097bc",
    "visible": true
}
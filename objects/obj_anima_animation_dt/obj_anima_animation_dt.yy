{
    "id": "b206dd7c-bdb2-430c-ab81-ed1aa612744f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_anima_animation_dt",
    "eventList": [
        {
            "id": "73f6e8f9-b441-4356-9566-814a0a646332",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b206dd7c-bdb2-430c-ab81-ed1aa612744f"
        },
        {
            "id": "ab5df9b6-7f64-4cab-9f83-6387177b23d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b206dd7c-bdb2-430c-ab81-ed1aa612744f"
        },
        {
            "id": "98083b67-0249-47cc-8fff-f6c9512d3974",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "b206dd7c-bdb2-430c-ab81-ed1aa612744f"
        },
        {
            "id": "830d6748-3116-45c8-9d3b-6f4777edf345",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "b206dd7c-bdb2-430c-ab81-ed1aa612744f"
        },
        {
            "id": "e65e7c21-7717-417e-89f5-09c6816fa751",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "b206dd7c-bdb2-430c-ab81-ed1aa612744f"
        },
        {
            "id": "30e99bf0-3160-4ec2-be1d-c273ad3c60aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "b206dd7c-bdb2-430c-ab81-ed1aa612744f"
        },
        {
            "id": "c9867a5b-027f-4f2f-b483-83dcf729581b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "b206dd7c-bdb2-430c-ab81-ed1aa612744f"
        },
        {
            "id": "19a8c69a-3ed1-4ff4-bd5d-a799fd84009c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "b206dd7c-bdb2-430c-ab81-ed1aa612744f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}
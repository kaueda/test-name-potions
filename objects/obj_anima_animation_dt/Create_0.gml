can_animate = false;

target_obj = noone;

initial_value = 0;
initial_value[0] = 0;
initial_value[1] = 0;

final_value = 0;
final_value[0] = 0;
final_value[1] = 0;

current_value = 0;
current_value[0] = 0;
current_value[1] = 0;
previous_value = 0;
previous_value[0] = 0;
previous_value[1] = 0;

value_difference = 0;
value_difference[0] = 0;
value_difference[1] = 0;

no_of_values = 1;
curr_value_no = 0;

easing = anima_ease_linear;
duration = 0;
timer = 0;

/*
no_of_start_events = 0;
start_events = 0;
start_events[0, 0] = noone; // Object
start_events[0, 1] = noone; // Event type
start_events[0, 2] = noone; // Event number

no_of_end_events = 0;
end_events = 0;
end_events[0, 0] = noone; // Object
end_events[0, 1] = noone; // Event type
end_events[0, 2] = noone; // Event number
*/

current_timed_event = 0;
first_timed_event_index = -1;
timed_event = 0;
timed_event[0, 0] = -1; // Time for this event to trigger
timed_event[0, 1] = -1; // Index of next event
timed_event[0, 2] = noone; // Object
timed_event[0, 3] = noone; // Event type
timed_event[0, 4] = noone; // Event number

loop = false;

destroy_after_use = false;

is_by = false;
paused = false;
looped_first_time = false;

i = 0;

has_pivot = false;
scale = 1;
pivot_obj = noone;
if(!instance_exists(target_obj)){
    instance_destroy();
}
else{
    if(can_animate && !paused){
        if(timer < duration){
            if((timer == 0) && !looped_first_time){            
                event_user(3);
            }
        
            for(curr_value_no = 0; curr_value_no < no_of_values; curr_value_no++){
                previous_value[curr_value_no] = current_value[curr_value_no];
                current_value[curr_value_no] = script_execute(easing, timer, initial_value[curr_value_no], value_difference[curr_value_no], duration);
            }       
            
            event_user(0);
            
            event_user(5);
            
            timer+= scale*delta_time;
        }
        else{
            for(curr_value_no = 0; curr_value_no < no_of_values; curr_value_no++){
                previous_value[curr_value_no] = current_value[curr_value_no];
                current_value[curr_value_no] = final_value[curr_value_no];
            }
            
            event_user(0);
            event_user(5);
            
            if(loop){
                if(!looped_first_time){
                    looped_first_time = true;
                }
                timer = 0;   
            }
            else{
                can_animate = false;
            }
            current_timed_event = first_timed_event_index;
            if(destroy_after_use){
                instance_destroy();                  
            }
        }
    }
}
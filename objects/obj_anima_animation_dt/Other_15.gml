/// @description Checking if there's an event to be triggered
if ((current_timed_event != -1) && (timed_event[current_timed_event, 0] <= timer)){
    i = current_timed_event;
    while ((i != -1) && (timed_event[i, 0] <= timer)){
        if(instance_exists(timed_event[i, 2])){
            with(timed_event[i, 2]){
                event_perform(other.timed_event[other.i, 3], other.timed_event[other.i, 4]);
            }
        }
        i = timed_event[i, 1];
        current_timed_event = i;
    }
}


/// @description Zooming The Camera
if(zooming){
    var wchange, hchange;
    var new_wvalue, new_hvalue;
	var cam_x = camera_get_view_x(camera);
	var cam_y = camera_get_view_y(camera);
	
    if(zooming_time_counter >= zooming_duration){
        zooming = false;        
		new_wvalue = final_w;
		new_hvalue = final_h;        
    } else {        
        new_wvalue = script_execute(easing_script, zooming_time_counter, initial_w, final_w - initial_w, zooming_duration);        
        new_hvalue = script_execute(easing_script, zooming_time_counter, initial_h, final_h - initial_h, zooming_duration)
    }
    
	wchange =  new_wvalue - camera_get_view_width(camera);
    hchange = new_hvalue - camera_get_view_height(camera);
	
	camera_set_view_size(camera, new_wvalue, new_hvalue);
	camera_set_view_pos(camera, cam_x - (wchange/2), cam_y - (hchange/2));
	
    zooming_time_counter++;
}
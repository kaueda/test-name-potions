/// @description Get initial values
if(!instance_exists(target_obj)){
    instance_destroy();
    exit;
}
initial_value[1] = target_obj.image_angle;
initial_x = target_obj.x;
initial_y = target_obj.y;
if(pivot_coords_are_absolute){
    initial_value[0] = degtorad( point_direction(rotation_point_x, rotation_point_y, target_obj.x, target_obj.y) );
    radius = point_distance(rotation_point_x, rotation_point_y, target_obj.x, target_obj.y);
}
else{
    if(!instance_exists(pivot_object)){
        initial_value[0] = degtorad( point_direction(rotation_point_x + target_obj.x, rotation_point_y + target_obj.y, target_obj.x, target_obj.y) );        
		radius = point_distance(rotation_point_x + target_obj.x, rotation_point_y + target_obj.y, target_obj.x, target_obj.y);
    }
    else{
        initial_value[0] = degtorad( point_direction(pivot_object.x, pivot_object.y, target_obj.x, target_obj.y) );
        radius = point_distance(pivot_object.x, pivot_object.y, target_obj.x, target_obj.y);
        previous_pivot_object_x = pivot_object.x;
        previous_pivot_object_y = pivot_object.y;        
    }    
    //final_value[1] += initial_value[1];
}
if(!is_by){
	if(has_point_of_rotation){
	    value_difference[0] = final_value[0] - initial_value[0];
	    final_value[1] = initial_value[1] + radtodeg(value_difference[0]);
	    value_difference[1] = final_value[1] - initial_value[1];
	}
	else{
		final_value[1] = radtodeg(final_value[0]);
		value_difference[1] = final_value[1] - initial_value[1];
	}
}
else{
    final_value[0] = initial_value[0] + value_difference[0];
    final_value[1] = initial_value[1] + value_difference[1];
}
show_debug_message("Initial angle: " + string(radtodeg(initial_value[1])) + ", diference: " + string(radtodeg(value_difference[1])) + ", final: " + string(radtodeg(final_value[1])));
event_inherited();


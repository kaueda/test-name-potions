/// @description Set animation target to initial state
if(!instance_exists(target_obj)){
    instance_destroy();
    exit;
}
target_obj.x = initial_x;
target_obj.y = initial_y;
target_obj.image_angle = initial_value[1];


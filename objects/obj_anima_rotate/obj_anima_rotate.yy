{
    "id": "9f7c9605-eaf6-40ee-8e42-d732c5551441",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_anima_rotate",
    "eventList": [
        {
            "id": "9f4cc8f0-998b-4a36-bfb2-5b4e337c8402",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "9f7c9605-eaf6-40ee-8e42-d732c5551441"
        },
        {
            "id": "96514174-f0cd-4d8c-bdb9-34a805b4344a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "9f7c9605-eaf6-40ee-8e42-d732c5551441"
        },
        {
            "id": "f27c8cd7-4880-4723-bb74-f8814221bbc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "9f7c9605-eaf6-40ee-8e42-d732c5551441"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b206dd7c-bdb2-430c-ab81-ed1aa612744f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}
/// @description Calculating values
if(instance_exists(target_obj)){
    if(instance_exists(pivot_object)){
        target_obj.x += pivot_object.x - previous_pivot_object_x;
        target_obj.y += pivot_object.y - previous_pivot_object_y;
        previous_pivot_object_x = pivot_object.x;
        previous_pivot_object_y = pivot_object.y;  
    }
    if(has_point_of_rotation){
        target_obj.x += radius*(cos(current_value[0]) - cos(previous_value[0]));
        target_obj.y -= radius*(sin(current_value[0]) - sin(previous_value[0]));               
    }
    target_obj.image_angle += current_value[1] - previous_value[1];
}
else{
    instance_destroy();
}




/// @description Setting values
if(instance_exists(target_obj)){
    target_obj.image_alpha += current_value[0] - previous_value[0];
    if(target_obj.object_index == obj_anima_fill_camera){
        if(target_obj.image_alpha <= 0){
            if(target_obj.visible){
                target_obj.visible = false;
            }
        }
        else{
            if(!target_obj.visible){
                target_obj.visible = true;
            }
        }
    }
}
else{
    instance_destroy();
}


if(!instance_exists(target_obj)){
    instance_destroy();
    exit;
}
if(!is_by){
    initial_value[0] = target_obj.image_alpha;
    value_difference[0] = final_value[0] - initial_value[0];
}
else{
    initial_value[0] = target_obj.image_alpha;
    final_value[0] = initial_value[0] + value_difference[0];
}
event_inherited();


{
    "id": "a544cb46-c807-4adb-b7bb-b27864c38664",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_anima_fade",
    "eventList": [
        {
            "id": "0f943ebe-67e4-4af1-9554-a5fa42dc41cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "a544cb46-c807-4adb-b7bb-b27864c38664"
        },
        {
            "id": "222d138a-21dc-41d9-99da-70f1dfd13ee9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "a544cb46-c807-4adb-b7bb-b27864c38664"
        },
        {
            "id": "46ef5245-b419-454d-8ae2-087980f0f87b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "a544cb46-c807-4adb-b7bb-b27864c38664"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b206dd7c-bdb2-430c-ab81-ed1aa612744f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}
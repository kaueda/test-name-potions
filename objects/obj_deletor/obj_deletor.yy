{
    "id": "be2c774a-b2ac-4a26-a4e8-dfc04368e2d6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_deletor",
    "eventList": [
        {
            "id": "6c2bd1d5-1ec9-4e04-8ea8-12075d04e291",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "be2c774a-b2ac-4a26-a4e8-dfc04368e2d6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8cd71750-3d6f-476a-a80f-d4a95519c364",
    "visible": true
}
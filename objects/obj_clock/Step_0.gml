/// @description Counting Step
if(counting){
	var passed = 0;
	
	_timer_value += delta_time;
	
	passed = _timer_value/1000000;
	
	if(passed >= interval){
		passed = floor(passed/interval);
		_timer_value = _timer_value%(1000000*interval);
		while(passed > 0){
			if(broadcaster && ds_exists(broadcast_list, ds_type_list)){
				script_execute(broadcast_script, id);
			} else {
				event_perform(nobroadcast_ev_type, nobroadcast_ev_number);
			}
			passed -= interval;
		}
	}
}
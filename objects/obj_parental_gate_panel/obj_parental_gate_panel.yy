{
    "id": "3cfa7b8b-1377-47d8-88a6-0ee763b7c98a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_parental_gate_panel",
    "eventList": [
        {
            "id": "99c258c5-9b5d-4e91-a1d7-571c99e290f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3cfa7b8b-1377-47d8-88a6-0ee763b7c98a"
        },
        {
            "id": "976c1f26-6af4-4f51-b2a6-ec8ddd6fa4f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "3cfa7b8b-1377-47d8-88a6-0ee763b7c98a"
        },
        {
            "id": "410d8c82-f26d-42cd-b624-ee60bc5b31d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3cfa7b8b-1377-47d8-88a6-0ee763b7c98a"
        },
        {
            "id": "e48a6169-45ab-4825-8d35-61724d8e4715",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "3cfa7b8b-1377-47d8-88a6-0ee763b7c98a"
        },
        {
            "id": "0bcb4133-5e1e-4d52-bc69-5969754c0aa8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3cfa7b8b-1377-47d8-88a6-0ee763b7c98a"
        },
        {
            "id": "f7cc278b-02a3-4d2e-8663-644f1da222cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "3cfa7b8b-1377-47d8-88a6-0ee763b7c98a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "20fea3b1-831a-4919-bef9-d02d07d7848d",
    "visible": true
}
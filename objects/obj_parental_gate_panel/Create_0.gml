/// @description Create Frame and Buttons

with(obj_button){
    enabled = false;
}

active = false;
site_to_go = noone;
activated_all = false;

fillAlpha = 0.8;
fadeDuration = 0.3;
backgroundDismiss = false;

x = room_width/2;
y = room_height/2 - 60;

image_alpha = 0;

backbtn = noone;
btn_01 = noone;
btn_02 = noone;
btn_03 = noone;

fill = anima_fill(c_black, 0, false, false);
fill.visible = true;

alarm[0] = 1;

/// @description Destroy and Release Buttons

with(backbtn){
    instance_destroy();
}
with(btn_01){
    instance_destroy();
}
with(btn_02){
    instance_destroy();
}
with(btn_03){
    instance_destroy();
}
with(fill){
    instance_destroy();
}
with(obj_button){
    enabled = true;
}
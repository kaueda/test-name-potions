/// @description Alarm to Create Everything
var half_h_panel = ceil(sprite_height/2);
var half_h_button = ceil(sprite_get_height(spr_parental_gate_back)/2);

fill.image_alpha = fillAlpha * image_alpha;
fill.depth = depth+1;

btn_01 = instance_create_depth(x - sprite_width/4, y + sprite_height/2 - 100, depth-1, obj_parental_gate_btn); //x + 106, y + 277
btn_02 = instance_create_depth(x, y + sprite_height/2 - 100, depth-1, obj_parental_gate_btn); //x + 270, y + 277
btn_03 = instance_create_depth(x + sprite_width/4, y + sprite_height/2 - 100, depth-1, obj_parental_gate_btn); //x + 434, y + 277

if(!backgroundDismiss){
	backbtn = instance_create_depth(x, y + half_h_panel + (room_height - (y + half_h_panel))/2, depth, obj_parental_gate_back);
	backbtn.image_alpha = image_alpha;
}

btn_01.image_alpha = image_alpha;
btn_02.image_alpha = image_alpha;
btn_03.image_alpha = image_alpha;

var myfade = anima_fade_to(id, 1, fadeDuration, false, false);
anima_add_finish_event(myfade, id, ev_other, ev_user0);

/// @description SetUp/Destroy

if(image_alpha >= 1){
	active = true;
	
	btn_01.enabled = true;
	btn_02.enabled = true;
	btn_03.enabled = true;

	if(!backgroundDismiss){
		backbtn.enabled = true;
	}
} else if(image_alpha <= 0){
	instance_destroy();	
}


/// @description Fade to Destroy

var myfade = anima_fade_to(id, 0, fadeDuration, false, false);
anima_add_finish_event(myfade, id, ev_other, ev_user0);

active = false;

btn_01.enabled = false;
btn_02.enabled = false;
btn_03.enabled = false;

if(!backgroundDismiss){
	backbtn.enabled = false;
}

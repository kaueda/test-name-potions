/// @description Check Buttons

if(active && btn_01.image_index == 1 && btn_02.image_index == 1 && btn_03.image_index == 1 && !activated_all){
    activated_all = true;
	
    btn_01.image_index = 0;
    btn_02.image_index = 0;
    btn_03.image_index = 0;
	
    btn_01.activating_device = 4;
    btn_02.activating_device = 4;
    btn_03.activating_device = 4;
	
    url_open_ext(site_to_go, "_blank");
	
	instance_destroy();
} else {
	var me = id;
	with(btn_01){
		image_alpha = me.image_alpha;
	}

	with(btn_02){
		image_alpha = me.image_alpha;
	}

	with(btn_03){
		image_alpha = me.image_alpha;
	}

	with(fill){
		image_alpha = me.fillAlpha * me.image_alpha;
	}

	if(!backgroundDismiss){
		backbtn.image_alpha = image_alpha;
	} else if(active){
		if(mouse_check_button_released(mb_left) && !position_meeting(mouse_x, mouse_y, id)){
			event_user(1); //Fade Out to Destroy
		}
	}
}

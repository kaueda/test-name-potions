/// @description Slider

if(can_slide){
	var x_ = 0;
	var y_ = 0;
    if(easing_timer > time){
		camera_set_view_pos(camera, dest_x, dest_y);
        instance_destroy();
    } else {
		x_ = script_execute(easing_script, easing_timer, initial_x, dest_x - initial_x, time);
		y_ = script_execute(easing_script, easing_timer, initial_y, dest_y - initial_y, time);
		
		camera_set_view_pos(camera, x_, y_);
        easing_timer++;
    }
}

/*x_ = script_execute(easing_script, time, initial_x, dest_x - initial_x, time);
y_ = script_execute(easing_script, time, initial_y, dest_y - initial_y, time);
camera_set_view_pos(camera, x_, y_);*/
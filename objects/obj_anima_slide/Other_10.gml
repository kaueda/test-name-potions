/// @description Setting values
if(instance_exists(target_obj)){
//    show_debug_message("current_value[0] : " + string(current_value[0]) + ", previous_value[0]: " + string(previous_value[0]) );
    target_obj.x += current_value[0] - previous_value[0];
    target_obj.y += current_value[1] - previous_value[1];
	if(has_pivot){
		target_obj.x -= current_value[2];
		target_obj.y -= current_value[3];
	}
}
else{
    instance_destroy();
}


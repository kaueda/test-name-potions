{
    "id": "160e5dc1-40d6-4ceb-b48f-3b90481669ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_anima_slide",
    "eventList": [
        {
            "id": "040d7436-b787-4c20-aef1-e595dffb9609",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "160e5dc1-40d6-4ceb-b48f-3b90481669ec"
        },
        {
            "id": "a6fe5b80-9599-45a3-8bc0-568a94292a58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "160e5dc1-40d6-4ceb-b48f-3b90481669ec"
        },
        {
            "id": "4ac40d23-7f14-4561-96d2-4286ab5dfd99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "160e5dc1-40d6-4ceb-b48f-3b90481669ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b206dd7c-bdb2-430c-ab81-ed1aa612744f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}
if(!instance_exists(target_obj)){
    instance_destroy();
    exit;
}
if(!is_by){
    initial_value[0] = target_obj.x;
    initial_value[1] = target_obj.y;
    value_difference[0] = final_value[0] - initial_value[0];
    value_difference[1] = final_value[1] - initial_value[1];
}
else{
    initial_value[0] = target_obj.x;
    initial_value[1] = target_obj.y;
    final_value[0] = initial_value[0] + value_difference[0];
    final_value[1] = initial_value[1] + value_difference[1];
}
event_inherited();


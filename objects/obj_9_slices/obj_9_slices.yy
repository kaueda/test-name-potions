{
    "id": "06c6a040-dc43-4466-8c54-40dcbb22323c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_9_slices",
    "eventList": [
        {
            "id": "75a2c3de-eae5-43fd-8c1a-b27d5135d6cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "06c6a040-dc43-4466-8c54-40dcbb22323c"
        },
        {
            "id": "52abba09-2be9-4786-a615-b5747203f6f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "06c6a040-dc43-4466-8c54-40dcbb22323c"
        },
        {
            "id": "166e3cf4-589a-4af1-8279-44e5bb9f9a92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "06c6a040-dc43-4466-8c54-40dcbb22323c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
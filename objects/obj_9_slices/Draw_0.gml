/// @description Draws all slices
if(can_draw){
    if(!single_sprite_mode){
        //1
        draw_sprite_ext(nine_slice_part_00, image_index, x, y, 1, 1, 0, image_blend, image_alpha);
        //2
        draw_sprite_ext(nine_slice_part_01, image_index, x + slice_2_x0, y, horizontal_strech_scale_factor, 1, 0, image_blend, image_alpha);
        //3
        draw_sprite_ext(nine_slice_part_02, image_index, x + slice_3_x0, y, 1, 1, 0, image_blend, image_alpha);
        
        //4
        draw_sprite_ext(nine_slice_part_10, image_index, x, y + slice_4_y0, 1, vertical_strech_scale_factor, 0, image_blend, image_alpha);
        //5
        draw_sprite_ext(nine_slice_part_11, image_index, x + slice_2_x0, y + slice_4_y0, horizontal_strech_scale_factor, vertical_strech_scale_factor, 0, image_blend, image_alpha);
        //6
        draw_sprite_ext(nine_slice_part_12, image_index, x + slice_3_x0, y + slice_4_y0, 1, vertical_strech_scale_factor, 0, image_blend, image_alpha);
        
        //7
        draw_sprite_ext(nine_slice_part_20, image_index, x, y + slice_7_y0, 1, 1, 0, image_blend, image_alpha);
        //8
        draw_sprite_ext(nine_slice_part_21, image_index, x + slice_2_x0, y + slice_7_y0, horizontal_strech_scale_factor, 1, 0, image_blend, image_alpha);
        //9
        draw_sprite_ext(nine_slice_part_22, image_index, x + slice_3_x0, y + slice_7_y0, 1, 1, 0, image_blend, image_alpha);
    }
    else{
        //1        
        draw_sprite_part_ext(nine_slice_part_00, image_index, 0, 0, single_part_width, single_part_height, x, y, 1, 1, image_blend, image_alpha);
        //2        
        draw_sprite_part_ext(nine_slice_part_00, image_index, single_part_width, 0, single_part_width, single_part_height, x + slice_2_x0, y, horizontal_strech_scale_factor, 1, image_blend, image_alpha);
        //3        
        draw_sprite_part_ext(nine_slice_part_00, image_index, 2*single_part_width, 0, single_part_width, single_part_height, x + slice_3_x0, y, 1, 1, image_blend, image_alpha);
        
        //4        
        draw_sprite_part_ext(nine_slice_part_00, image_index, 0, single_part_height, single_part_width, single_part_height, x, y + slice_4_y0, 1, vertical_strech_scale_factor, image_blend, image_alpha);
        //5        
        draw_sprite_part_ext(nine_slice_part_00, image_index, single_part_width, single_part_height, single_part_width, single_part_height, x + slice_2_x0, y + slice_4_y0, horizontal_strech_scale_factor, vertical_strech_scale_factor, image_blend, image_alpha);
        //6        
        draw_sprite_part_ext(nine_slice_part_00, image_index, 2 * single_part_width, single_part_height, single_part_width, single_part_height, x + slice_3_x0, y + slice_4_y0, 1, vertical_strech_scale_factor, image_blend, image_alpha);
        
        //7        
        draw_sprite_part_ext(nine_slice_part_00, image_index, 0, 2*single_part_height, single_part_width, single_part_height, x, y + slice_7_y0, 1, 1, image_blend, image_alpha);
        //8        
        draw_sprite_part_ext(nine_slice_part_00, image_index, single_part_width, 2*single_part_height, single_part_width, single_part_height, x + slice_2_x0, y + slice_7_y0, horizontal_strech_scale_factor, 1, image_blend, image_alpha);
        //9        
        draw_sprite_part_ext(nine_slice_part_00, image_index, 2*single_part_width, 2*single_part_height, single_part_width, single_part_height, x + slice_3_x0, y + slice_7_y0, 1, 1, image_blend, image_alpha);
    }
}
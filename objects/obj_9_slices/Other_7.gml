/// @description Stops Animation if PlayOneTime
if(animate_just_one_time){
    image_speed = 0;
    image_index = image_number - 1;
}


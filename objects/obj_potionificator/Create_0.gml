att_map = ds_map_create();
att_noun_tier_map = ds_map_create();
att_adjs_tier_map = ds_map_create();

ds_map_add(att_noun_tier_map, 0, global.fire_nouns); ds_map_add(att_adjs_tier_map, 0, global.fire_adjs);
ds_map_add(att_noun_tier_map, 1, global.water_nouns);ds_map_add(att_adjs_tier_map, 1, global.water_adjs);
ds_map_add(att_noun_tier_map, 2, global.earth_nouns);ds_map_add(att_adjs_tier_map, 2, global.earth_adjs);
ds_map_add(att_noun_tier_map, 3, global.air_nouns);  ds_map_add(att_adjs_tier_map, 3, global.air_adjs);

for (var i = 0; i < global.n_atts; i++) {
	ds_map_add(att_map, i, 0);
}

for (var i = global.n_atts - 1; i >= 0; i--) {
	attributes[i] = 0;
}

display_name = "Nothingness Potion";


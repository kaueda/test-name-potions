{
    "id": "50590373-bc8a-417d-a50d-5799cb3e699a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_potionificator",
    "eventList": [
        {
            "id": "fd322a26-78b8-4d92-923c-f32c578a2a1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "50590373-bc8a-417d-a50d-5799cb3e699a"
        },
        {
            "id": "fd1247e2-308c-42ec-aff2-dbd3697cd78d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "50590373-bc8a-417d-a50d-5799cb3e699a"
        },
        {
            "id": "03a62e73-a1a5-4dd1-bd6a-7f69ba18c084",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "50590373-bc8a-417d-a50d-5799cb3e699a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8cd71750-3d6f-476a-a80f-d4a95519c364",
    "visible": true
}
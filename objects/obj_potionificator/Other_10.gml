var use_map = true
#region With Map
att_map[? last_added_att] += last_added_att_val;

// Check Order of Attributes Values Ascending
var _ordered_att = scr_map_sorted_keys(att_map);

// [ToDo] Check for Attribute mix

if (att_map[? _ordered_att[0]] > 0) {
	// Check for Biggest Attribute (noun)
	for (var i = 0; i < global.atts_n_tiers; i++) {
		if (att_map[? _ordered_att[0]] > 2 * global.fibbs_rates[i]) {
			continue;
		} else {
			var noun_arr = att_noun_tier_map[? _ordered_att[0]];
			var noun = noun_arr[i];
			break;
		}
	}
	
	// Check order for the remaining Attributes (adjs)
	var adjs = "";
	//for (var i = 1; i < global.n_atts; i++) {
	for (var i = global.n_atts-1; i > 0; i--) {
		if (att_map[? _ordered_att[i]] > 0) {
			for (var j = 0; j < global.atts_n_tiers; j++) {
				if (att_map[? _ordered_att[i]] > 2 * global.fibbs_rates[j]) {
					continue;
				} else {
					var adjs_arr = att_adjs_tier_map[? _ordered_att[i]];
					adjs += adjs_arr[j] + " ";
					break;
				}
			}
		}
	}
	
	// Build potion name
	show_debug_message(adjs + noun + " Potion");
}
#endregion

if (!use_map) {
	#region With Arrays
	var att_index;
	att_index[global.n_atts - 1] = -1;

	#region Setup about 
	var bigger_att = 0;
	for (var i = 0; i < global.n_atts; i++) {
		att_index[i] = -1;
		if (attributes[i] > 0) {
			if (attributes[bigger_att] < attributes[i]) {
				bigger_att = i;
			}
	
			// Select Attribute Tier
			for (var j = 0; j < global.atts_n_tiers; j++) {
				if (attributes[i] > 2 * global.fibbs_rates[j]) {
					continue;
				} else {
					att_index[i] = j;
					break;
				}
			}
		}
	}
	#endregion

	display_name = "";
	var noun = "";
	var adjs = "";
	for (var i = 0; i < global.n_atts; i++) {
		if (att_index[i] > -1) {
			switch (i) {
				#region Fire Attribute
				case 0:
					if (bigger_att == i) noun = global.fire_nouns[att_index[i]];
					else adjs += global.fire_adjs[att_index[i]] + " ";
				break;
				#endregion
			
				#region Water Attribute
				case 1:
					if (bigger_att == i) noun = global.water_nouns[att_index[i]];
					else adjs += global.water_adjs[att_index[i]] + " ";
				break;
				#endregion
			
				#region Earth Attribute
				case 2:
					if (bigger_att == i) noun = global.earth_nouns[att_index[i]];
					else adjs += global.earth_adjs[att_index[i]] + " ";
				break;
				#endregion
			
				#region Air Attribute
				case 3:
					if (bigger_att == i) noun = global.air_nouns[att_index[i]];
					else adjs += global.air_adjs[att_index[i]] + " ";
				break;
				#endregion
			}
		}
	}
#endregion
}

if (noun != "")	display_name = adjs + noun + " Potion";
else display_name = "Nothingness Potion";

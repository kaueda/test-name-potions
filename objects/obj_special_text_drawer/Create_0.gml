/// @description Create

baseline_modifier_factor = 0.2;
percentage_to_interletter_spacing = 0.2;
text_total_height = 0;
surface_source_left = 0;
surface_source_top = 0;
//percentage_to_interletter_spacing = 0;

debug = false;

i = 0;
j = 0;
current_char_at_text_input = 1;
width = 500;
height = 500;
line_spacing = 0;

ammount_of_modifiers = -1;
horizontal_accumulator = 0;
vertical_accumulator = 0;
string_font_name = "";
text_string_width = 0;
text_string_height = 0;

text_color = c_black;
text_x0 = 224;
text_y0 = 96;


text_font = noone;
base_text_font_name = "";
text_valign = fa_top;
text_halign = fa_left;
justified = false;
type_of_text_align = 1; // 0: left, 1: center, 2: right
can_draw = false;

input_string = "";
text_string = 0;

no_of_lines = 0;
line_specs = 0;
possible_line_break_index = -1;
width_until_this_break = 0;

line_specs_set = false;
text_drawn = false;
/*
line_specs[0, 0] = 16;
line_specs[0, 1] = 0;
line_specs[0, 5] = 2;
line_specs[1, 0] = -1;
line_specs[1, 1] = 0;
line_specs[1, 5] = 0;
*/

word_count = 0;
white_space_ammount = 0;
read_char = "";
previous_read_char = "";

interpreted_string_segment = 0;
current_line = 0;
has_to_check_for_font_changes = true;
baseline_modifier = 0;
read_modifier = "";
word_count = 0;
letter_count = 0;
line_breaks = 0;
line_breaks[0] = -1;
current_line_break_index = 0;

curr_inline_imgs = 0;
inline_imgs[0, 0] = noone;
inline_imgs[0, 1] = noone;
inline_imgs[0, 2] = -1;
inline_imgs[0, 3] = 0;
inline_imgs[0, 4] = 0;
inline_imgs[0, 5] = 0;
inline_imgs[0, 6] = 0;
inline_imgs[0, 7] = 0;
/*
line_specs[0, 0] = Index to break line;
line_specs[0, 1] = Margin left;
line_specs[0, 2] = Margin right;
line_specs[0, 3] = Letter spacing;
line_specs[0, 4] = Distance between words;
line_specs[0, 5] = Whitespaces at end;
line_specs[0, 6] = Line content;

text_string[0, 0] = Last char index that this modifier has effect;
text_string[0, 1] = Modifier 1;
text_string[0, 2] = Modifier 2;
text_string[0, 3] = Modifier 3;
                .
                .
                .
text_string[0, n] = Modifier n;

line_breaks[0] = Index of line break number 0;
line_breaks[1] = Index of line break number 1;
                .
                .
                .
line_breaks[n] = Index of line break number n;

inline_imgs[0, 0] = Image 0 sprite;
inline_imgs[0, 1] = Image 0 object;
inline_imgs[0, 2] = String position to insert Image 0;
inline_imgs[0, 3] = Image 0 sprite width;
inline_imgs[0, 4] = Image 0 sprite height;
inline_imgs[0, 5] = Image 0 sprite x offset;
inline_imgs[0, 6] = Image 0 sprite y offset;
inline_imgs[0, 7] = Image 0 sprite animation duration;
                
ammount_of_strings = array_height_2d(text_string);
*/

surface_container = id;
text_surface = noone;
text_sprite = noone;

/* */
/*  */

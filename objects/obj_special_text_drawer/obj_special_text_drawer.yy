{
    "id": "21f622ae-78c0-47c5-bbe9-a1a0585e0c71",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_special_text_drawer",
    "eventList": [
        {
            "id": "4e501e9e-dc7e-4648-83fe-e87c90f78ce1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "21f622ae-78c0-47c5-bbe9-a1a0585e0c71"
        },
        {
            "id": "e65dbcd4-9a13-4978-af8a-eb599892759c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "21f622ae-78c0-47c5-bbe9-a1a0585e0c71"
        },
        {
            "id": "8f9f3d25-e445-4b83-aca3-bcb25fbf6d52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "21f622ae-78c0-47c5-bbe9-a1a0585e0c71"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
/// @description Draw Text

if(can_draw && !text_drawn){    

    draw_set_color(text_color);
    draw_set_valign(text_valign);
    draw_set_halign(text_halign);
    horizontal_accumulator = 0;
    vertical_accumulator = 0;    
    current_line = 0;        
    interpreted_string_segment = 0;
    var inline_img_index, inline_img_x, inline_img_y;
    curr_inline_imgs = 0;

    for(current_char_at_text_input = 1; current_char_at_text_input <= string_length(input_string); current_char_at_text_input++){
        
        /****************************************************************************
        *
        *   Routine to set text font based on current char being read
        *
        ***************************************************************************/  
        if(current_char_at_text_input > text_string[interpreted_string_segment, 0]){
            interpreted_string_segment++;
            has_to_check_for_font_changes = true;
        }
        if(has_to_check_for_font_changes || current_char_at_text_input == 1){
            ammount_of_modifiers = array_length_2d(text_string, interpreted_string_segment);
            string_font_name = base_text_font_name;
            baseline_modifier = 0;
            text_valign = fa_top;            
            for(j = 1; j < ammount_of_modifiers; j++){
                read_modifier = text_string[interpreted_string_segment, j];
                if(read_modifier == "^"){                    
                    read_modifier = "s";
                    baseline_modifier =  text_string_height * baseline_modifier_factor;
                    text_valign = fa_middle;
                }
                else if(read_modifier == "_"){                    
                    read_modifier = "s";
                    baseline_modifier = text_string_height * (1 - baseline_modifier_factor);
                    text_valign = fa_middle;
                }                
                string_font_name += read_modifier;
            }
            draw_set_valign(text_valign);
            draw_set_font(asset_get_index(string_font_name));
            has_to_check_for_font_changes = false;
        }
        /**************************************************************************/

        if(line_specs_set){
            /****************************************************************************
            *
            *   Routine to draw text        
            *
            ***************************************************************************/
            if(!surface_exists(text_surface)){
                text_surface = surface_create(width, text_total_height);
                surface_set_target(text_surface);      
                draw_clear_alpha(c_white, 0);
            }

            
            if(current_char_at_text_input == -1) || (current_char_at_text_input != line_specs[current_line, 0]){
                previous_read_char = read_char;
                read_char = string_char_at(input_string, current_char_at_text_input);
                if(current_line != (array_height_2d(line_specs) - 1)) && (line_breaks[current_line_break_index] != current_line) && (horizontal_accumulator != 0) {
                    if(previous_read_char != " " && read_char == " "){
                        horizontal_accumulator += line_specs[current_line, 6];
                    }
                    if(previous_read_char != " " && read_char != " "){
                        horizontal_accumulator += line_specs[current_line, 7];
                    }
                }

                if(curr_inline_imgs != -1 && current_char_at_text_input == inline_imgs[curr_inline_imgs, 2] ){
                    inline_img_index = inline_imgs[curr_inline_imgs, 0];
                    inline_img_x = x + horizontal_accumulator + line_specs[current_line, 1] + inline_imgs[curr_inline_imgs, 5];
                    inline_img_y = y + vertical_accumulator + inline_imgs[curr_inline_imgs, 6] + text_string_height;                    
                    inline_imgs[curr_inline_imgs, 1] = instance_create_depth(inline_img_x, inline_img_y, depth, obj_special_text_img);
                    inline_imgs[curr_inline_imgs, 1].sprite_index = inline_img_index;
                    inline_imgs[curr_inline_imgs, 1].image_speed = inline_imgs[curr_inline_imgs, 1].image_number/(real(inline_imgs[curr_inline_imgs, 7]) * room_speed);
                    inline_imgs[curr_inline_imgs, 1].father_image = id;
                    inline_imgs[curr_inline_imgs, 1].relative_x_pos = inline_imgs[curr_inline_imgs, 1].x - x;
                    inline_imgs[curr_inline_imgs, 1].relative_y_pos = inline_imgs[curr_inline_imgs, 1].y - y;
                    horizontal_accumulator += inline_imgs[curr_inline_imgs, 3];
                    curr_inline_imgs++;
                    if(curr_inline_imgs >= array_height_2d(inline_imgs)){
                        curr_inline_imgs = -1;
                    }
                }
                else{                
                    draw_text(horizontal_accumulator + line_specs[current_line, 1], vertical_accumulator + baseline_modifier, string_hash_to_newline(read_char));
                    horizontal_accumulator += string_width(string_hash_to_newline(read_char));                 
                }

                
                if(debug){
                    draw_set_colour(c_blue);
                    draw_line(horizontal_accumulator + line_specs[current_line, 1], vertical_accumulator + baseline_modifier, horizontal_accumulator + line_specs[current_line, 1] + string_width(string_hash_to_newline(read_char)), vertical_accumulator + baseline_modifier);
                    draw_line(horizontal_accumulator + line_specs[current_line, 1] + string_width(string_hash_to_newline(read_char)), vertical_accumulator + baseline_modifier, horizontal_accumulator + line_specs[current_line, 1] + string_width(string_hash_to_newline(read_char)), vertical_accumulator + baseline_modifier + string_height(string_hash_to_newline(read_char)));
                    draw_line(horizontal_accumulator + line_specs[current_line, 1] + string_width(string_hash_to_newline(read_char)), vertical_accumulator + baseline_modifier + string_height(string_hash_to_newline(read_char)), horizontal_accumulator + line_specs[current_line, 1], vertical_accumulator + baseline_modifier + string_height(string_hash_to_newline(read_char)));
                    draw_line(horizontal_accumulator + line_specs[current_line, 1], vertical_accumulator + baseline_modifier + string_height(string_hash_to_newline(read_char)), horizontal_accumulator + line_specs[current_line, 1], vertical_accumulator + baseline_modifier);
                    draw_set_color(text_color);
                    
                }
            }
            else{
                if(line_breaks[current_line_break_index] == current_line){
                    if(current_line_break_index < array_length_1d(line_breaks) -1){
                        current_line_break_index++;
                    }
                    else{
                        current_line_break_index = 0;
                    }                    
                }           
                current_char_at_text_input+=line_specs[current_line, 5] - 1; // Jumping white spaces at the beggining of line
                current_line++;
                if(line_spacing == -1){
                    vertical_accumulator += text_string_height;
                }
                else{
                    vertical_accumulator += line_spacing;
                }
                horizontal_accumulator = 0;                
            }        
            
            if(current_char_at_text_input == string_length(input_string)){
                if(debug){
                    draw_set_colour(c_green);
                    draw_line(0, 0, 0 + width - 1, 0);
                    draw_line(0 + width - 1, 0, 0 + width - 1, 0 + height - 1);
                    draw_line(0 + width -1 , 0 + height - 1, 0, 0 + height - 1);
                    draw_line(0, 0 + height - 1, 0, 0);                                        
                    draw_set_color(text_color);
                }

                text_sprite = sprite_create_from_surface(text_surface, 0, 0, width, text_total_height, false, false, 0, 0);
                surface_reset_target();
                surface_free(text_surface);
                text_drawn = true;
            }                
            /**************************************************************************/
        }
        else{
            /****************************************************************************
            *
            *   Routine to calculate justification offsets       
            *
            ***************************************************************************/
            var is_last_char_of_string;
            var ammount_of_blank_spaces;
            
            // line_specs[no_of_lines, 0] = 0;            
            ammount_of_blank_spaces = 0;                    
            
            previous_read_char = read_char;
            read_char = string_char_at(input_string, current_char_at_text_input);
            if(previous_read_char != " " && read_char == " "){
                possible_line_break_index = current_char_at_text_input;
                width_until_this_break = text_string_width;
                word_count++;                        
            }
            if(read_char != " "){
                letter_count++;
            }                

            if(curr_inline_imgs!= -1 && current_char_at_text_input == inline_imgs[curr_inline_imgs, 2]){
                text_string_width += inline_imgs[curr_inline_imgs, 3];
                curr_inline_imgs++;
                if(curr_inline_imgs >= array_height_2d(inline_imgs)){
                    curr_inline_imgs = -1;
                }
            }
            else{
                text_string_width += string_width(string_hash_to_newline(read_char));            
            }
            is_last_char_of_string = (current_char_at_text_input == string_length(input_string));
            if(debug)
                show_debug_message("Read char '" + read_char + "', curr width: '" + string(text_string_width) + "', max width '" +  string(width));
            if ( ((text_string_width > width) && (possible_line_break_index != -1)) || is_last_char_of_string || ((current_line_break_index != -1) && (line_breaks[current_line_break_index] == current_char_at_text_input)) ){ // Has to continue next line                    
                if(current_line_break_index != -1) && (line_breaks[current_line_break_index] == current_char_at_text_input){
                    line_breaks[current_line_break_index] = current_line;
                    if(current_line_break_index < array_length_1d(line_breaks) -1){
                        current_line_break_index++;
                    }
                    else{
                        current_line_break_index = -1;
                    }
                }
                
                if(is_last_char_of_string && (text_string_width > width)){
                    is_last_char_of_string = false;
                }
                
                if (!is_last_char_of_string){
                    if(possible_line_break_index == -1){
                        possible_line_break_index = line_specs[current_line-1, 0];                        
                    }                    
                    line_specs[current_line, 0] = possible_line_break_index;                    
                    white_space_ammount = width - width_until_this_break;                        
                    ret_array = trim_white_spaces(input_string, 0, ma(possible_line_break_index, string_length(input_string)));
                    ammount_of_blank_spaces = ret_array[1];
                    current_char_at_text_input = possible_line_break_index - 1 + ammount_of_blank_spaces;
                    read_char = " ";
                    possible_line_break_index = -1;
                }
                else{                           
                    line_specs[current_line, 0] = -1;                                     
                    white_space_ammount = width - text_string_width;                     
                    ammount_of_blank_spaces = 0;
                }
                if (is_last_char_of_string){
                    word_count++;
                }
                if(type_of_text_align == 0){
                    line_specs[current_line, 1] = 0;
                    line_specs[current_line, 2] = white_space_ammount;
                    line_specs[current_line, 6] = 0;
                    line_specs[current_line, 7] = 0;
                }
                else if(type_of_text_align == 1){
                    line_specs[current_line, 1] = white_space_ammount/2;
                    line_specs[current_line, 2] = white_space_ammount/2;
                    line_specs[current_line, 6] = 0;
                    line_specs[current_line, 7] = 0;
                }
                else if(type_of_text_align == 2){
                    line_specs[current_line, 1] = white_space_ammount;
                    line_specs[current_line, 2] = 0;                    
                    line_specs[current_line, 6] = 0;
                    line_specs[current_line, 7] = 0;
                }
                else if(type_of_text_align == 3){
                    line_specs[current_line, 1] = 0;
                    line_specs[current_line, 2] = 0;                    

                    if(word_count > 1){
                        line_specs[current_line, 6] = ((1 - percentage_to_interletter_spacing) * white_space_ammount)/(word_count - 1);
                    }
                    else{
                        line_specs[current_line, 6] = 0;
                    }
                    //line_specs[current_line, 7] = (percentage_to_interletter_spacing * white_space_ammount)/(letter_count - word_count);
                    line_specs[current_line, 7] = word_count;
                    line_specs[current_line, 8] = white_space_ammount;
                }
                line_specs[current_line, 5] = ammount_of_blank_spaces;                 
                word_count = 0;
                letter_count = 0;
                ammount_of_blank_spaces = 0;                                   
                current_line++;                                        
                text_string_width = 0;
            }
            
            if (is_last_char_of_string){
                line_specs_set = true;
                curr_inline_imgs = 0;
                if(debug){
                    show_debug_message("Line breaks:");
                    for(j = 0; j < array_length_1d(line_breaks); j++){
                        show_debug_message("Line break nro (" + string(j) + "): " + string(line_breaks[j]));
                    }
                }
                current_line_break_index = 0;
                draw_set_font(asset_get_index(base_text_font_name));
                text_string_height = string_height(string_hash_to_newline(input_string));
                text_total_height = current_line * text_string_height;
                if(height == -1){
                    height = text_total_height;
                }
                if(callback_obj != noone){
                    with(callback_obj){
                        event_perform(other.callback_type, other.callback_no);
                    }
                }
                if(debug){
                    show_debug_message("Line specs:");
                }
                if(type_of_text_align == 3){
                    current_line = 0;
                    for(j = 1; j <= string_length(input_string); j++){
                        if(j != line_specs[current_line, 0]){
                            read_char = string_char_at(input_string, j);
                            if(read_char != " "){
                                letter_count++;
                            }
                        }
                        else{
                            // line_specs[current_line, 7] = letter_count - line_specs[current_line, 7];
                            line_specs[current_line, 7] = (percentage_to_interletter_spacing * line_specs[current_line, 8])/(letter_count - line_specs[current_line, 7]);
                            letter_count = 0;
                            current_line++;
                        }
                    }
                }
                if(debug){
                    for(j = 0; j < array_height_2d(line_specs); j++){
                        show_debug_message("Line no.: " + string(j) + ", index to break line: " + string(line_specs[j, 0]) + ".");
                    }
                }
                read_char = " ";                         
            }
            /**************************************************************************/
        }
    }    
}
else if(text_drawn){
    //draw_surface_ext(text_surface, x, y, image_xscale, image_yscale, image_angle, image_blend, image_alpha);
    draw_sprite_ext(text_sprite, 0, x, y, image_xscale, image_yscale, image_angle, image_blend, image_alpha);
    /*
    if(!surface_exists(text_surface)){
        text_drawn = false;
    }
    else{ 
        //draw_surface_part( text_surface, surface_source_left, surface_source_top, width, height, text_x0, text_y0);
        if(surface_container == id){
            //draw_sprite_ext(text_sprite, 0, text_x0, text_y0, image_xscale, image_yscale, image_angle, image_blend, image_alpha);
            draw_surface_ext(text_surface, text_x0, text_y0, image_xscale, image_yscale, image_angle, image_blend, image_alpha);
            //draw_surface_part_ext( text_surface, surface_source_left, surface_source_top, width, height, text_x0, text_y0, image_xscale, image_yscale, image_blend, image_alpha);
        }
        else{
            if(surface_exists(surface_container.surface)){
                surface_set_target(surface_container.surface);
                //draw_sprite_ext(text_sprite, 0, text_x0, text_y0, image_xscale, image_yscale, image_angle, image_blend, image_alpha);
                draw_surface_ext(text_surface, text_x0, text_y0, image_xscale, image_yscale, image_angle, image_blend, image_alpha);
                //draw_surface_part_ext( text_surface, surface_source_left, surface_source_top, width, height, text_x0, , image_xscale, image_yscale, image_blend, image_alpha);
                surface_reset_target();
            }
        }
    }
    */
}

/* */
/*  */

/// @description Clean Up

if(surface_exists(text_surface)){
    surface_free(text_surface);
}
if(sprite_exists(text_sprite)){
    sprite_delete(text_sprite);
}
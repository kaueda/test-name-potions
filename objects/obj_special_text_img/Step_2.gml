/// @description Fix Position based on Parent

if(instance_exists(father_image)){
    x = father_image.x + relative_x_pos;
    y = father_image.y + relative_y_pos;
    depth = father_image.depth - 1;
    image_alpha = father_image.image_alpha;
    visible = father_image.visible;
}


/// @description Shake it
if(can_shake){
	var xoffset, yoffset;
	
    if(shake_time == 0){
        camera_set_view_pos(camera, initial_x, initial_y);
        instance_destroy();
        
		exit;
    }
	
	xoffset = initial_x + random_range(- max_shake_distance, max_shake_distance);
	yoffset = initial_y + random_range(- max_shake_distance, max_shake_distance);
	
	camera_set_view_pos(camera, xoffset, yoffset);
    max_shake_distance -= shake_dec_factor;
    shake_time--;
}


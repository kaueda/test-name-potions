{
    "id": "868515c7-ba07-443b-8078-f7d47aec9d1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_parental_gate_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfee1bfa-f748-4b8d-8dd9-6fdc8347d4f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "868515c7-ba07-443b-8078-f7d47aec9d1b",
            "compositeImage": {
                "id": "9ae756f1-7e8f-4d37-94ab-f3f82433f2db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfee1bfa-f748-4b8d-8dd9-6fdc8347d4f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00e5e801-c99f-4226-8d6c-bf5901be1ab7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfee1bfa-f748-4b8d-8dd9-6fdc8347d4f4",
                    "LayerId": "7fd0c5de-c0c1-450e-88f2-ba02a9bdd70a"
                }
            ]
        },
        {
            "id": "f7388f95-9ccd-4dac-bbbd-131ee1405904",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "868515c7-ba07-443b-8078-f7d47aec9d1b",
            "compositeImage": {
                "id": "46a04f28-3e19-4b66-81e8-be8d9d2e9804",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7388f95-9ccd-4dac-bbbd-131ee1405904",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0c20d67-8120-43c1-83b9-5ef18547150d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7388f95-9ccd-4dac-bbbd-131ee1405904",
                    "LayerId": "7fd0c5de-c0c1-450e-88f2-ba02a9bdd70a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "7fd0c5de-c0c1-450e-88f2-ba02a9bdd70a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "868515c7-ba07-443b-8078-f7d47aec9d1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}
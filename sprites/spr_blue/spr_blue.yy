{
    "id": "334567ba-8564-4402-b876-66c68b716fcb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afcaeaae-8778-4ef4-bff3-815ff1bc49a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "334567ba-8564-4402-b876-66c68b716fcb",
            "compositeImage": {
                "id": "6b87a074-19ec-47b4-88de-b979928a7a0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afcaeaae-8778-4ef4-bff3-815ff1bc49a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cdda92e-5fa1-447d-980e-e6277fa4075f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afcaeaae-8778-4ef4-bff3-815ff1bc49a8",
                    "LayerId": "ed458128-03c1-424f-9eae-57d25a881e92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ed458128-03c1-424f-9eae-57d25a881e92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "334567ba-8564-4402-b876-66c68b716fcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
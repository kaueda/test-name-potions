{
    "id": "833c4a94-73a8-4911-b961-c39de91097bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_voltar_creditos_music",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 0,
    "bbox_right": 105,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35a2a6a1-6ff9-41f8-b9a6-cd9536508f09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "833c4a94-73a8-4911-b961-c39de91097bc",
            "compositeImage": {
                "id": "4d484d2e-4f32-491b-b422-55c63422dd5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35a2a6a1-6ff9-41f8-b9a6-cd9536508f09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44ead92c-f5bf-4019-9429-2b77a3c43536",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35a2a6a1-6ff9-41f8-b9a6-cd9536508f09",
                    "LayerId": "4c32cae5-9f93-4d11-9c64-5dc63bbd7483"
                }
            ]
        },
        {
            "id": "8642de8e-5733-48e5-8705-d829f6ca1af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "833c4a94-73a8-4911-b961-c39de91097bc",
            "compositeImage": {
                "id": "e5d2df03-42a9-48fb-ad73-c95463014af2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8642de8e-5733-48e5-8705-d829f6ca1af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2de55742-29c0-4983-83b0-f92e18907e81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8642de8e-5733-48e5-8705-d829f6ca1af7",
                    "LayerId": "4c32cae5-9f93-4d11-9c64-5dc63bbd7483"
                }
            ]
        },
        {
            "id": "909b405a-39f0-4971-be1c-59239914944e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "833c4a94-73a8-4911-b961-c39de91097bc",
            "compositeImage": {
                "id": "e48153ae-ba2b-4712-80f7-899d23d33491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "909b405a-39f0-4971-be1c-59239914944e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "632a88ea-612e-43dd-94ed-b8590357921e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "909b405a-39f0-4971-be1c-59239914944e",
                    "LayerId": "4c32cae5-9f93-4d11-9c64-5dc63bbd7483"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "4c32cae5-9f93-4d11-9c64-5dc63bbd7483",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "833c4a94-73a8-4911-b961-c39de91097bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 106,
    "xorig": 53,
    "yorig": 53
}
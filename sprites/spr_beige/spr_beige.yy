{
    "id": "8cd71750-3d6f-476a-a80f-d4a95519c364",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_beige",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f43f4c9-648f-4bb8-8d8a-7dcc2d811b95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cd71750-3d6f-476a-a80f-d4a95519c364",
            "compositeImage": {
                "id": "a5a55e7e-9ab8-41b6-a5a3-c7b9a8ad7e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f43f4c9-648f-4bb8-8d8a-7dcc2d811b95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df3faef3-90f3-4b29-bdcb-b16872be058e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f43f4c9-648f-4bb8-8d8a-7dcc2d811b95",
                    "LayerId": "1df0b36e-39a1-4bc0-bb33-cdc5dcaa2515"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1df0b36e-39a1-4bc0-bb33-cdc5dcaa2515",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8cd71750-3d6f-476a-a80f-d4a95519c364",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
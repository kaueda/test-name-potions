{
    "id": "867a8915-4b2d-4bea-bfdf-2ae084abd16c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_roi",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a056b19a-1c40-48a1-8c34-a85a96777556",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "867a8915-4b2d-4bea-bfdf-2ae084abd16c",
            "compositeImage": {
                "id": "da106ce0-6dab-4a28-968f-a8198ea56fde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a056b19a-1c40-48a1-8c34-a85a96777556",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad3cc7eb-f7da-48aa-9f10-e772c02ee4ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a056b19a-1c40-48a1-8c34-a85a96777556",
                    "LayerId": "4e0ee8fa-ff23-44ac-877c-9cf90adc8e98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "4e0ee8fa-ff23-44ac-877c-9cf90adc8e98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "867a8915-4b2d-4bea-bfdf-2ae084abd16c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}
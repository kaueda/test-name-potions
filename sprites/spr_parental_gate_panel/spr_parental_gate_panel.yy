{
    "id": "20fea3b1-831a-4919-bef9-d02d07d7848d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_parental_gate_panel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 426,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "287f8d0a-210a-4789-afd5-cd1a03fe3a39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20fea3b1-831a-4919-bef9-d02d07d7848d",
            "compositeImage": {
                "id": "bc7483c7-5c8f-4543-a690-5a248078f8ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "287f8d0a-210a-4789-afd5-cd1a03fe3a39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac99e862-5d39-46dd-bd7e-09a94cde02c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "287f8d0a-210a-4789-afd5-cd1a03fe3a39",
                    "LayerId": "9f61ae44-08d3-47fa-b1cc-97e5d3475923"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 427,
    "layers": [
        {
            "id": "9f61ae44-08d3-47fa-b1cc-97e5d3475923",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20fea3b1-831a-4919-bef9-d02d07d7848d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 213
}
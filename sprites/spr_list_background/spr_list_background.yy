{
    "id": "1e4fcdeb-2212-46f0-9ca9-7d59abb387dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_list_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05a18bc9-1af6-41d7-aac8-3a2e7d2139ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e4fcdeb-2212-46f0-9ca9-7d59abb387dc",
            "compositeImage": {
                "id": "f3152a28-fda0-4ef8-bd86-aa2e682ab4a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05a18bc9-1af6-41d7-aac8-3a2e7d2139ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22774fd7-40aa-43e4-b35b-3cfc20fab38e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05a18bc9-1af6-41d7-aac8-3a2e7d2139ec",
                    "LayerId": "43358704-0e7f-45ed-acf2-4dcbfefa1cac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "43358704-0e7f-45ed-acf2-4dcbfefa1cac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e4fcdeb-2212-46f0-9ca9-7d59abb387dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}